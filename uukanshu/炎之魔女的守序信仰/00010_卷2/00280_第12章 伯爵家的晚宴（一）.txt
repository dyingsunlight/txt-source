“感觉今晚会很开心呢~~”

少爷用着悠闲的语气，走进空无一人的大厅。

“对贵族们来说确实是宴会，下人们可要遭殃了呢。”

我推着装满甜点的餐车，把点心逐一取下放在指定的位置上。

“别这麼说嘛，我不也在帮着你吗？”

“妾身也只是开玩笑~~想必这个宅邸没有会对伯爵有怨言的仆从吧。”

更何况少爷也是一直在帮着我的忙。

“这个可可饼乾味道很不错啊，要来一块吗？”

少爷拿起了盘子中的一块烤饼乾向我的嘴边递过来。

“唔~~好好吃。啊，不对！！现在不能偷吃啦，再说这不是您做的嘛。”

“嘿嘿~~不过和在厨房裡试吃是完全不同的感觉啊。”

“哪有这种事啊！”

回想起来，那天告白之後少爷就没有再次提过了，这是怎麼回事？

一点也不期待我的回应……该不会只是想戏弄我吧。

那样的话实在是恶劣的玩笑啊，简直就像是过去那个青梅竹马一样。

不……玩弄别人的感情比单纯地拒绝要恶劣多了。

不过不管處於怎样的目的，现在的我还是不想轻易接受这样的表白，少爷还是孩子……而且我也不明白自己在这方面到底该怎麼办。

作为精神的男性喜欢上女孩？表面上的同性恋者。

作为别人眼中的普通情侣喜欢上男孩？精神上的同性恋者。

无论哪一个都不是能随便选择的呢。

这麼长时间作为女孩子生活虽说已经没什麼违和感了，但是一想到要作为女生被男人吻……然後被做些下流的事就总感觉莫名的抵触。

艾利欧格大人那次的强吻倒是完全没有太大的感觉……大概是当时的目的性很强吧，没时间在意这些小事。

也许身体再长大一点到了第二性征期时就会受女性的费洛蒙影响自然被男性吸引？那样的话这种情绪也许变淡一些。

“唔……妾身在想什麼危险的事情啊。”

“诶？什麼什麼~~”

“什麼都没有啦。”

“克莉斯……生气了？”

“完全没有，少爷。宴会没多久就要开始了，继续留在这裡可是不行的哦。”

“嗯~~一会就去準备。”

面对我指导般的说辞。少爷并没有什麼反抗的情绪，在这段时间他也成长了很多呢。

琳达他们现在应该已经在大门口静候客人的光临了。

而我因为年纪太小所以被留下来做杂事。

“那麼我先去老爹那裡了哟，克莉斯一个人没问题吗？”

“当然，别忘了妾身是少爷您的女仆哦，哪有让主人担心的女仆啊。”

“说的也是~~”

回头向我挥了挥手暂时告别之後，少爷便离开了大厅。

“好了~~差不多摆放完了呢。”

由於是立食宴会，所以食物都已经分类排好了……只要把甜品漂亮的摆在一处即可。

来访的贵族多达50人左右，所有也事先也有雇佣了一些侍者。

但是平时伯爵大人是不喜欢太多下人在宅邸裡乱窜的，所以家裡才只有5个仆人吧。

不过现在这裡实在是太过於空空荡荡了，对於一会就要聚集大量客人的事情还是没有什麼实感呢。之前在希尔芙利亚做公主的时候，每一次自己都是宴会的中心，出现的时候有大人们的注视和献媚。而现在现在居然要来服侍这些人，命运真是不可思议。

将已经空掉的餐车慢慢推回厨房。

雇来的调酒师和侍者们正在将饮料倒入擦得闪闪发亮的玻璃杯中。

“主菜已经都準备好了，大家可以就位了哟。”

“哦，明白了~~女仆小姐。”

“年纪很小但是幹活很麻利嘛。”

“诶嘿嘿~~”

“那麼，这边的果实酒就拜托小姑娘了哦。”

“嗯，交给妾身吧。”

感觉大家都是很亲切的人呢，虽然说也许不过是被这幅外表影响了而已。

无论怎麼说，还是保持平常心吧。考虑太多消极的事情笑容也会失去色彩的。

将装着果实酒的托盘拿回大厅的时候，已经有客人们陆续到达了。

琳达和管家大叔已经回到了大厅中。梅阿莉和艾露想必还留在门口吧，如果是她们两个就不需要担心了。

渐渐也热闹起来了呢。

在水晶吊灯的柔和光线映射下，各位大人正在为着自己的目的攀谈着，无一不穿着华丽的他们上至须发鬓白的老人，下到由年轻夫人引来的刚进入社交界的少年少女。

“那边的女仆，给我一杯果实酒。”

“是，是的。”

唤住我是一个长着雀斑的红髮少年，看起来十三四岁的样子，大概和尤朵處於同一世代。

这种年纪的孩子喝酒没问题吗？

当然我不会自讨没趣说这种教，毕竟现在身份有别，我只要做好女仆的工作就好了。

“请慢用……”

将一杯酒递给少年，放下托盘向他鞠了一躬。

“唔……好难喝。”

啜了一口後，少年立即皱起了眉头将剩下的酒倒掉。

看上去不是很中意酒精的味道呢。

嘛，我是觉得还不错来着，这种酒就像果汁一样，只有一点点苦涩的感觉。

“喂！让我喝这麼难喝的东西，你是故意耍我吗？”

“欸？不，十分抱歉……大人，这酒大概就是这种味道。”

这死小孩……喝不惯就别喝啊。

故意找茬吗？

“哈？你是女仆吧，要喊我主人才对哦。”

“欸……抱歉，妾身有真正的主人。”

“你说什麼，你敢得罪我吗？知不知道我是谁啊……”

混蛋一个而已，可惜又不能在这轰飞。

“真的是非常抱歉，如果可以的话妾身帮您换一杯好入口的饮料可以吗？”

虽然我已经最大限度地让步了，但是少年的眼睛裡却完全没有妥协的意思。

这孩子的家长在哪啊！都不出来管教一下吗？

这种场合丢脸的可是你们的家族哦。

“现在已经不是这种问题了，我可是韦伯斯特侯爵家的长子。你弄坏了本少爷的心情，打算怎麼赔偿啊？”

“就算您这麼说……”

韦伯斯特大概是伊弗尔德有名的一族，以前曾经有学习过。

确实不算无名的贵族，证据就是我环顾了一下四周之後，只有个别的贵族向这边督了一眼，但很快又装作什麼都没看见一样继续聊天了。

“不过你挺可爱的嘛。这样吧，本少爷大人有大量，你把裙子卷起来给我看看我就当什麼这件失礼的事没有發生过好了。”

“！！”

实在是差劲透了……连巷子裡的流－氓都不如。

“喂！听见了吗？快把裙子卷起来！！”

他的语气变得凶狠起来，一副目中无人的态度，估计从小就在难以想象的优渥中成长。

才会有这样目中无人的狰狞面目吧。

不过现在我也是完全的手足无措，有不能揍他一顿泄愤，扩大事态会给伯爵添麻烦。

最後只好这样了吗？委屈一下自己。

算了，反正也不会掉块肉……而且原本还是男人来着，没什麼大不了的。

我将两手捏在短裙上。

可以感受到缓缓地提起裙摆的手臂在颤抖着……

可恶，感觉好不甘心！！

一点也不想给这种人看。

“你想给自己的家族抹黑吗？”

“欸？”

就在我与内心搏斗的时候一道年轻的男子声音在身後响起。

这个过於熟悉的声音让我甚至不敢转身往後看。

“你这傢伙是谁啊？敢管本少爷的闲事！”

“韦伯斯特家可是有名的骑士世家，知道做出这种欺负女孩子的事。你的父亲知道可是饶不了你的哦。”

“唔……你少拿父亲威胁我，他现在正在和莎法尔伯爵谈话呢！”

“嚯~~莎法尔伯爵看来已经换了谈话对象了呢。”

仿佛顺着男子的目光，少年看到了一个气势汹汹的大叔正向自己这边过来。

“呃……这次就放过你们，给我记住！！”

说完杂鱼般的败北台词，少年就一溜烟般的逃走了。

“欸……”

我松了一口气。

那麼，接下来该怎麼办呢。

虽说避免了卷裙子一劫，但是後面这傢伙也很棘手。

我甚至连回头感谢他也做不到。

“小姐，你没事吧。”

做什麼都义正言辞的感觉，还有这种凛然的声音……我所知道的人裡只有一个。

……真正的危机是这边吗？！