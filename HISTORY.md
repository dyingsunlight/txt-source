# HISTORY

## 2019-04-04

### Epub

- [異世界迷宮の最深部を目指そう](user_out/%E7%95%B0%E4%B8%96%E7%95%8C%E8%BF%B7%E5%AE%AE%E3%81%AE%E6%9C%80%E6%B7%B1%E9%83%A8%E3%82%92%E7%9B%AE%E6%8C%87%E3%81%9D%E3%81%86) - user_out
  <br/>( v: 28 , c: 404, add: 0 )

### Segment

- [異世界迷宮の最深部を目指そう](user/%E7%95%B0%E4%B8%96%E7%95%8C%E8%BF%B7%E5%AE%AE%E3%81%AE%E6%9C%80%E6%B7%B1%E9%83%A8%E3%82%92%E7%9B%AE%E6%8C%87%E3%81%9D%E3%81%86) - user
  <br/>( s: 15 )

## 2019-04-03

### Epub

- [書姬吟游錄](wenku8/%E6%9B%B8%E5%A7%AC%E5%90%9F%E6%B8%B8%E9%8C%84) - wenku8
  <br/>( v: 4 , c: 19, add: 19 )

## 2019-03-28

### Epub

- [アラフォー賢者の異世界生活日記](syosetu/%E3%82%A2%E3%83%A9%E3%83%95%E3%82%A9%E3%83%BC%E8%B3%A2%E8%80%85%E3%81%AE%E7%95%B0%E4%B8%96%E7%95%8C%E7%94%9F%E6%B4%BB%E6%97%A5%E8%A8%98) - syosetu
  <br/>( v: 1 , c: 70, add: 70 )

### Segment

- [アラフォー賢者の異世界生活日記](syosetu/%E3%82%A2%E3%83%A9%E3%83%95%E3%82%A9%E3%83%BC%E8%B3%A2%E8%80%85%E3%81%AE%E7%95%B0%E4%B8%96%E7%95%8C%E7%94%9F%E6%B4%BB%E6%97%A5%E8%A8%98) - syosetu
  <br/>( s: 13 )

## 2019-03-25

### Epub

- [傀儡契約_(n4719ff)](girl/%E5%82%80%E5%84%A1%E5%A5%91%E7%B4%84_(n4719ff)) - girl
  <br/>( v: 1 , c: 2, add: 1 )
- [死憶の異世界傾国姫　～究極の悲劇のヒロインめざしてTS転生～](syosetu_out/%E6%AD%BB%E6%86%B6%E3%81%AE%E7%95%B0%E4%B8%96%E7%95%8C%E5%82%BE%E5%9B%BD%E5%A7%AB%E3%80%80%EF%BD%9E%E7%A9%B6%E6%A5%B5%E3%81%AE%E6%82%B2%E5%8A%87%E3%81%AE%E3%83%92%E3%83%AD%E3%82%A4%E3%83%B3%E3%82%81%E3%81%96%E3%81%97%E3%81%A6TS%E8%BB%A2%E7%94%9F%EF%BD%9E) - syosetu_out
  <br/>( v: 3 , c: 48, add: 3 )

### Segment

- [傀儡契約_(n4719ff)](girl/%E5%82%80%E5%84%A1%E5%A5%91%E7%B4%84_(n4719ff)) - girl
  <br/>( s: 1 )

## 2019-03-24

### Epub

- [転生！異世界より愛をこめて](syosetu/%E8%BB%A2%E7%94%9F%EF%BC%81%E7%95%B0%E4%B8%96%E7%95%8C%E3%82%88%E3%82%8A%E6%84%9B%E3%82%92%E3%81%93%E3%82%81%E3%81%A6) - syosetu
  <br/>( v: 5 , c: 43, add: 43 )

## 2019-03-23

### Epub

- [勇者は犯されたい](h_out/%E5%8B%87%E8%80%85%E3%81%AF%E7%8A%AF%E3%81%95%E3%82%8C%E3%81%9F%E3%81%84) - h_out
  <br/>( v: 1 , c: 49, add: 0 )
- [レベル１の最強賢者](syosetu/%E3%83%AC%E3%83%99%E3%83%AB%EF%BC%91%E3%81%AE%E6%9C%80%E5%BC%B7%E8%B3%A2%E8%80%85) - syosetu
  <br/>( v: 2 , c: 5, add: 5 )
- [用務員さんは勇者じゃありませんので](syosetu_out/%E7%94%A8%E5%8B%99%E5%93%A1%E3%81%95%E3%82%93%E3%81%AF%E5%8B%87%E8%80%85%E3%81%98%E3%82%83%E3%81%82%E3%82%8A%E3%81%BE%E3%81%9B%E3%82%93%E3%81%AE%E3%81%A7) - syosetu_out
  <br/>( v: 4 , c: 63, add: 0 )
- [異世界迷宮の最深部を目指そう](user_out/%E7%95%B0%E4%B8%96%E7%95%8C%E8%BF%B7%E5%AE%AE%E3%81%AE%E6%9C%80%E6%B7%B1%E9%83%A8%E3%82%92%E7%9B%AE%E6%8C%87%E3%81%9D%E3%81%86) - user_out
  <br/>( v: 28 , c: 404, add: 9 )

## 2019-03-22

### Epub

- [Genocide Online ～極惡千金的玩遊戲日記～](girl/Genocide%20Online%20%EF%BD%9E%E6%A5%B5%E6%83%A1%E5%8D%83%E9%87%91%E7%9A%84%E7%8E%A9%E9%81%8A%E6%88%B2%E6%97%A5%E8%A8%98%EF%BD%9E) - girl
  <br/>( v: 1 , c: 60, add: 4 )
- [エリナの成長物語（引き籠り女神の転生）](girl/%E3%82%A8%E3%83%AA%E3%83%8A%E3%81%AE%E6%88%90%E9%95%B7%E7%89%A9%E8%AA%9E%EF%BC%88%E5%BC%95%E3%81%8D%E7%B1%A0%E3%82%8A%E5%A5%B3%E7%A5%9E%E3%81%AE%E8%BB%A2%E7%94%9F%EF%BC%89) - girl
  <br/>( v: 1 , c: 3, add: 1 )
- [你這種傢伙別想打贏魔王](girl_out/%E4%BD%A0%E9%80%99%E7%A8%AE%E5%82%A2%E4%BC%99%E5%88%A5%E6%83%B3%E6%89%93%E8%B4%8F%E9%AD%94%E7%8E%8B) - girl_out
  <br/>( v: 15 , c: 115, add: 1 )
- [美腿寶箱怪](h/%E7%BE%8E%E8%85%BF%E5%AF%B6%E7%AE%B1%E6%80%AA) - h
  <br/>( v: 1 , c: 1, add: 1 )
- [２９歳独身は異世界で自由に生きた…かった。](user_out/%EF%BC%92%EF%BC%99%E6%AD%B3%E7%8B%AC%E8%BA%AB%E3%81%AF%E7%95%B0%E4%B8%96%E7%95%8C%E3%81%A7%E8%87%AA%E7%94%B1%E3%81%AB%E7%94%9F%E3%81%8D%E3%81%9F%E2%80%A6%E3%81%8B%E3%81%A3%E3%81%9F%E3%80%82) - user_out
  <br/>( v: 13 , c: 120, add: 10 )
- [不吉波普系列](wenku8/%E4%B8%8D%E5%90%89%E6%B3%A2%E6%99%AE%E7%B3%BB%E5%88%97) - wenku8
  <br/>( v: 6 , c: 47, add: 0 )

### Segment

- [エリナの成長物語（引き籠り女神の転生）](girl/%E3%82%A8%E3%83%AA%E3%83%8A%E3%81%AE%E6%88%90%E9%95%B7%E7%89%A9%E8%AA%9E%EF%BC%88%E5%BC%95%E3%81%8D%E7%B1%A0%E3%82%8A%E5%A5%B3%E7%A5%9E%E3%81%AE%E8%BB%A2%E7%94%9F%EF%BC%89) - girl
  <br/>( s: 1 )
- [你這種傢伙別想打贏魔王](girl/%E4%BD%A0%E9%80%99%E7%A8%AE%E5%82%A2%E4%BC%99%E5%88%A5%E6%83%B3%E6%89%93%E8%B4%8F%E9%AD%94%E7%8E%8B) - girl
  <br/>( s: 11 )
- [２９歳独身は異世界で自由に生きた…かった。](user/%EF%BC%92%EF%BC%99%E6%AD%B3%E7%8B%AC%E8%BA%AB%E3%81%AF%E7%95%B0%E4%B8%96%E7%95%8C%E3%81%A7%E8%87%AA%E7%94%B1%E3%81%AB%E7%94%9F%E3%81%8D%E3%81%9F%E2%80%A6%E3%81%8B%E3%81%A3%E3%81%9F%E3%80%82) - user
  <br/>( s: 55 )
- [不吉波普系列](wenku8/%E4%B8%8D%E5%90%89%E6%B3%A2%E6%99%AE%E7%B3%BB%E5%88%97) - wenku8
  <br/>( s: 10 )



