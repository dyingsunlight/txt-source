# English

[TOC]

## 00010_一章/00020_所謂的盜賊就是會行走的送分關卡吧

### 'guard position'

- '躍起。Guard Position什麼的'

### 'short sleeper'

- '眠者【Short Sleeper】，為'

### 'sparring partner'

- '對象【Sparring Partner】一年'

### 'slime bodysuit'

- '身衣【Slime Bodysuit】。\n'

### 'slime'

- '萊姆【Slime】的身'

### 'black bodysuit'

- '身衣【Black bodySuit】。沒'

### 'slime sword'

- '姆劍【Slime Sword】還有'


## 00010_一章/00030_ディアボロス教団の爆誕！？

### 'shadowguardian'

- '影守【ShadowGuardian】⋯⋯'

### 'paly'

- '實力者Paly由複數'


## 00010_一章/00040_如果頭髮還在的話……

### 'paly'

- '實力者Paly，所以'


## 00010_一章/00050_真教團的人，和真被詛咒的人(真実風味)

### 'neglige'

- '袍下「Neglige」能夠'

### 'stretch'

- '體嘈【Stretch（伸展'


## 00010_一章/00060_藥檢結果、陽性

### 'shadow garden'

- '影守【Shadow Garden】」\n'


## 00010_一章/00070_影之實力者式簡單戰鬥教程（如此一來你也能成為影之實力者！？）

### 'lesson1'

- '\n\n「Lesson1」\n\n'

### 'lesson2'

- '\n\n「Lesson2」\n\n'

### 'lesson3'

- '\n\n「Lesson3」\n\n'


## 00010_一章/00090_不知世界陰暗面之人的嬉戲

### 'staccato'

- '斷音【Staccato】作為'

### 'vibrato'

- '顫音【Vibrato】上下'


## 00010_一章/00110_作為局外者看看糾紛還是挺有意思的

### 'mass'

- '之後是Mass組隊吧'
- '所謂的Mass指的是'
- '練吧。Mass終究只'


## 00010_一章/00130_順帶一提我是ゼノン派的

### 'get da'

- '萬澤尼Get Da☆'

### 'ze'

- 'Ze。\n我'


## 00010_一章/00160_シャドウ様戦記完全版執筆中

### 'wine glasses'

- '酒杯【Wine Glasses】被仿'

### 'antique lamp'

- '古燈【Antique Lamp】照的'

### 'pr'

- '奏曲【PrＭ'

### 'ude'

- 'Ude】就由'

### 'nocturne'

- '夜曲【Nocturne】奏響'


## 00010_一章/00190_遠い記憶

### 'rounds'

- '圓桌【Rounds】第１'

### 'knight of rounds'

- '騎士【Knight Of Rounds】。無'


## 00010_一章/00200_『アイ・アム……』

### 'am atomic'

- '、Ｉ Am Atomic』【注'
- '『Ｉ Am Atomic』\n\n'

### 'atomic'

- '導彈【Atomic】。嚴'


## 00020_二章/00230_異世界総合商社ミツゴシ商会

### 'placard'

- '牌的【Placard】、穿'


## 00020_二章/00300_〇〇野郎

### 'children1st'

- '曾經是Children1st的'


## 00020_二章/00310_你能跟得上我嗎？

### 'bloody tornado'

- '受身（Bloody Tornado）』\n'


## 00020_二章/00340_龍套也有不得不上的時候

### 'heart・break・mob'

- '體驗（Heart・Break・Mob）』\n'


## 00020_二章/00350_從屋頂上俯瞰而下的浪漫

### 'occasion'

- '合『Occasion』】\n'


## 00030_三章/00440_聖剣エクスカリバー

### 'excalibur'

- '瞥我的Excalibur了嗎」'
- '蚓還是Excalibur根本無'
- '那個是Excalibur。確定'


## 00030_三章/00590_不可避の一撃

### 'all range taepodpng'

- '「⋯⋯All Range Taepodpng」\n\n'

### 'am・all range taepodpng'

- '『Ｉ・Am・All Range Taepodpng』\n\n'


## 00050_五章/00890_門衛Ａ什麼的真是不錯啊！

### 'roast beef'

- '牛肉【Roast Beef】什麼'


## 00050_五章/00900_那個已經痊愈了啦

### 'ghoul'

- '屍鬼（Ghoul）⋯⋯'


## 00050_五章/01110_Mission Complete

### 'recovery atomic'

- '「⋯⋯Recovery Atomic」\n\n'


## 00050_五章/01120_畢竟右手很疼嘛、這也是沒辦法的事

### 'recovery'

- '扔出了Recovery，就當'


## 00060_六章/01130_不要！不要那麼粗暴！

### 'logo'

- '標志【Logo】。標'


## 00060_六章/01140_超級精英特工，其名為……

### 'fbi'

- '\n一副FBI特工【'

### 'fbiagent'

- 'FBIagent】的感'


## 00060_六章/01150_世界在對我輕語

### 'mhk2'

- '前世的MHK2小時紀'

### 'mhk'

- '信賴的MHK ２小時'


## 00060_六章/01180_黃金獵犬

### 'bo'

- '\n\n「Bo⋯⋯！'


## 00060_六章/01270_黒いジャガ

### 'secret'

- '秘密的Secret任務」'
- '秘密的Secret任務」'
- '秘密的Secret任務就'
- '秘密的Secret任務就'
- '秘密的Secret任務？'


## 00060_六章/01350_一切都如他所料

### 'secret'

- '秘密的Secret任務⋯'
- '秘密的Secret任務什'
- '秘密的Secret任務所'


## 00060_六章/01420_背叛夢想的鮮血淋漓的魔王

### 'secret'

- '秘密的Secret任務？'

### 'dying message'

- '信息【Dying Message】」\n'

### 'voice recorde'

- '音器【Voice Recorde】的話'


## 00070_七章/01450_【書籍発売記念閑話】彼の名は影野ミノル【シドの前世】

### '4750px'

- '應該有4750px的彪形'

### 'need more power'

- '「Ｉ Need More Power⋯⋯」'


## 00070_七章/01490_code 0

### 'code'

- '\n\n「Code ０」\n'
- '「ｃ、Code、０？'


## 00070_七章/01520_迫近的惡意

### 'slipping away'

- '卸力【Slipping Away】的技'


## 00070_七章/01600_差不多該我出場了吧？

### 'triple・tornado・slash'

- '斬擊【Triple・Tornado・Slash】！！'

### 'sway back spin'

- '回旋【Sway Back Spin】！！'

### 'butterfly step'

- '步法【Butterfly Step】！！'

### 'death spiral'

- '螺旋【Death Spiral】！！'


## 00070_七章/01650_魔王之力

### 'am atomic rain'

- '「Ｉ Am Atomic Rain」\n\n'


## 00080_八章/01670_我所目標的影之實力者

### 'atomic rain'

- '。在用Atomic Rain把收容'
