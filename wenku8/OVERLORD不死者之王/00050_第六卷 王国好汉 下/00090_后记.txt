
那麼，这第六集从各种意义来说，真是越看越糟糕啊。各位读者觉得呢？

以作者丸山的角度来看，会觉得这才符合《OVERLORD》的风格。如果各位也深有同感，作者会很高兴的。毕竟这些行径啊，一般的轻小说主角可是绝对不干的呢。

从前几集就安排好伏笔，到这时候就可以一脸得意地说「我从那时候就想好了呢」，会让我有点想炫耀一下。只是，如果安排得太明显又会被看穿……真难呢。我想最难看懂的应该是日记那裡，主角提到的是在第二集的某个场面翻过东西。考虑到袭击者的目的，其实没什麼理由翻东西，但反过来想，毕竟那个人下手的方式很夸张嘛。翻东西的时候就算搞得夸张点也不奇怪吧。不过东西没被翻得太多。就好像那人早就知道东西在哪裡了……真是狡猾啊。

就像这样，读过这次的故事後，如果再重新读过前面几集，也许会有意外的發现。

再来讲到登场人物，第五、六集的MVP绝对是伊维尔哀没错，不过我个人喜欢的角色，是直到最後才终於讲出名字的盗贼。只要是会把「年轻真好」挂在嘴边的读者，应该能体会丸山的心情。

事情就是这样，感谢读者赏光阅读了上下篇。对於各位有什麼样的感想，丸山非常感兴趣。虽然不好意思得请各位负担邮票钱，但如果大家愿意寄读者回函过来，我会很高兴的。

那麼，以下容我向各位人士致谢。

so-bin大人、F田大人、大迫大人、Chord　Design　Studio，以及协力制作《OVERLORD》的各方人士，感谢大家。还有Honey，谢谢你多多帮忙。

最後感谢买下本书的各位读者。真的很谢谢大家！

二〇一四年一月　　丸山くがね
