

「如果利昂大人作出了正确的行为而使我受罚，那麼，我将会欣然接受。」
米莉那样说着-不，正是因为这样对我说的米丽，所以希望今後也能相伴在我身旁。
如此这样的强烈期盼着。但是全部都看克莱尔薇蒂娜的心情，无法离开这栋宅邸的我什麼也办不到，度过了郁闷的每一天。
然後过了三天，我原先认为又是个平淡的一日，我跟米莉在房间内和红茶的时候，突然，一位女仆过来了。
黑髮黑瞳的女仆小姐，当然我并没有见过。
「……您是哪位呢？」
原本坐在椅子上的米丽站了起来，若无其事的为了庇护我而缓缓的移动。
「突然来访，十分抱歉。我是服侍克莱尔薇蒂娜的女仆，名叫米楔尔。」
「那麼米楔尔小姐，来这栋宅邸有什麼事对吧？这裡应该除了被允许的人都禁止进入才对。」
「确实是那样没错，不过先打破这项规矩不是你们吗？」
「……什麼事情？」
「请不要那麼警惕，我只是想跟利昂大人说一些些话。」
「刚才也说了，这裡不是没有许可的人可以来的地方，请您回去吧。」
「等一下，米莉。」
我站了起来挽住米莉的袖子。
「……利昂大人？」
「对不起，米莉，我想和他说话看看。」
「但是……不，我明白了，如果这是利昂大人做的决定，那我也不多说什麼了。」
米莉转到横侧等候，於是我面对着自称米楔尔的女仆。
「你是利昂大人吧？」
「是的，这样的话我也知道你是克莱尔薇蒂娜的随行女仆了，不过，找我有什麼事呢？」
以闯入这栋宅邸的行为来看应该不是卡罗琳女士派来的使者，虽然应该不会是最坏的展开，但米楔尔各式各样的情感隐藏在心中而目的无法预测。（这句是指他面无表情的意思，应该是三无女仆吧）
我以彷佛听到任何内容都不会动摇似的矗立着。
「在传达讯息前，可以先让我打听一件事情吗？」
「好…但是要问什麼阿？」
「对於姐弟之间的婚约你感兴趣吗？」
「……什麼？对不起，我好像有点听错了，再说一次好吗。」
「对於姐弟之间的婚约你感兴趣吗？」
「不明白意义！?」
「姐弟之间發生肉体关係，所谓的在性方面的――」（翻译君：人帅真好）
「不是不明白字面上的意义，为什麼会突然说到这种话阿！?说起来，怎麼对小孩子说这种话！?真是的。」
「不是什麼奇怪的话吧，确实在平民之间很少有，对贵族们却应该是常见之事。」
「是，是这样的吗？」
真不愧是异世界阿…不，不是那样的。在以前的地球上也是十分普通的事情，所以考虑到这个世界的文化水平并不是不可思议。
「利昂大人不知道吗？」
「我这样的孩子应该不会知道吧？」
「与预想中不同，不知道婚约的意思吗？」
「――就是这样！」
「而且，虽然从克莱尔大人听到时是半信半疑的，不过，奇怪的像大人般的说话方式呢。」
「这个是…」
糟糕了阿，因为只能跟米莉说话，最近并没有伪装成小孩子的语气。我不是什麼普通的孩子若传到卡罗琳女士的耳裡，肯定更多方面都会碍手碍脚的吧，这点无论如何都想瞒混过去。
「即使不做出那样的表情，我也不会将这裡的事说出去的，放心吧。」
「……那是什麼意思？」
「前幾天的事并没有报告，这次的对话也没有打算说的意思。」
「您相信他所说的吗？」（估计是米莉讲的）
「至少，这三天也没有發生什麼事。」
「……的确。」
如果传进了卡罗琳女士的耳裡的话，早就开始行动了吧。却到今天为止一切均安，克莱尔薇蒂娜的沉默就是最好的证据。
「是吗。所以过了幾天之後才过来阿。」
「……能到那样的理解，真的很厲害。我认为克莱尔大人也是相当成熟的人……你真的是个孩子吗，我很怀疑阿。」
「利昂大人十分聪明阿」
米莉如此自豪的说着。但实际上也靠是19年又加上4年的人生经验所以才能思考到这地步阿。要是是真的23岁的话，会有着更可靠的感觉……但是，不不不，我已经算是不怎麼学习的人了。
事到如今，到现在我也因动摇而做出许多荒唐的事情来。幸好我沉默着，让我在自己的思路中想办法。
（这句话不确定 求大神指教 原文：……って、今更か。俺も动揺していろ色やらかしたしな。幸い黙っててくれるみたいだし、味方に付ける方向で何とかしよう。）
「我觉得这已经不只是聪明之类的水平了，但是利昂大人的人格是不用怀疑的。也许就是所谓的天才吧」（开始乱翻了233333）
不，只是转生者。――什麼的嘴巴裂开也不能说，为了回避这项问题装做一副不知道的样子。
「已经了解她跟你都沉默着，你的目的是什麼？」
「在传递这个讯息之前，可以先请您回答刚才的问题吗？」
「刚才的问题？」
「所以，对於姐弟之间的婚约你感兴趣吗？」
「……这是认真的问题吗？」
「当然是真心问的，所以，你有兴趣吗？」
不可能有的吧，或该说，这样的问题从来没有去思考过。
「如果没有考虑过的话，今後会接受的可能性……」
「不，不会」
前世的我与纱弥互相支撑着生活，从他人来看，也许像是夫妻之间的关係，但在那裡面的爱情不是恋情。
所以我连姐姐和妹妹都不敢细想。
「……是吗，我明白了。」
「我完全不明白问题的意义。」
米楔尔以若无其事的脸接受了尖锐的挖苦。完全不明白这个人究竟在思考些什麼。
「总之，既然你理解了的话，能把此行的目的告诉我了吗？」
「阿，是的。第一个是，幾天前所發生的事表示感谢」
「是指治疗伤口的事情吗？」
「是的。大小姐固执的怎麼样也不让在下治疗，所以真的得救了，到底是怎麼样说服的呢？」
「没说什麼大不了的话阿。只是如果有细菌进入的话伤口就会化脓了」
「如果有细菌进入的话…吗？与大小姐那听说的一模一样，究竟的怎样的意思呢？」
「……嗯？阿，这样说的话」
克莱尔薇蒂娜是孩子所以不能理解呢，没想到这个世界医疗水平还挺低的？怪不得说不通吧。
「嗯~,你知道伤口化脓的情形吗？」
「嗯。这件事我还是知道的。」
「其原因就是细菌。」
「所以如果阻止细菌的话就不会化脓了吗？」
「没错。或着使用乾净的水可以把细菌冲走。」
「原来如此阿……听说受到严重的外伤的时候有使用酒精清洗伤口的习俗，那也是同样的理由吗？」
……风俗习惯阿。只有这点程度的认知而已阿。比想像中的医疗水平还要低。看来以後要十分注意不能生病或受伤。
「阿……理由是一样的，但最好不要使用酒精比较好」
「是这样吗？」（翻译君：原来用酒精不会比生理食盐水好吗？我也不知道呢…）
「嗯，乾净的水，如果可以的话稀薄的盐水清洗伤口是最理想的，之後常常洗使伤口不乾燥，很快就能痊癒了」
（注：这裡的盐水就是指生理食盐水，原文两个都有写 感觉稀薄的盐水比较符合时代背景就这麼写了）
这是最近在地球上才知道的事实。所以虽然在这个世界上会被误解，但是消毒与乾燥反而会使伤口的治癒速度减慢。
「十分的详细呢，谢谢你告诉我如此宝贵的知识。」
「这没什麼大不了的，不要在意。」
「目前不算什麼了不起的事吗？很在意你在哪裡学到了这样的知识，看来还是不要问的好吧」
「这麼做的话，我十分感激。」
「我了解了。那麼接下来是最後的事情。」
「欸欸米楔尔，到底要等到什麼时候阿？」
米楔尔的话像为了遮掩一般，门被打开，飘然飞舞的银色在眼前出现了。（翻译君：後面为了漂亮一点用脑补的 还是不优美没办法 我尽力了=3=）
「大小姐，我不是叮嘱你在我的呼叫之前，先待在门外等候吗」
「米楔尔太担心了阿，不是说了弟弟君是可以信任的吗」
米楔尔面向流露出不满声音的主人，那银色是克莱尔薇蒂娜的长髮。
「……为什麼你？」
「欸嘿，来了呦」
「不，早就来了吧。卡罗琳女士不是说连和我做朋友都不行吗？来这个地方，没问题吗？」
「是呐，我想母亲大人如果知道的话一定会十分盛怒吧，所以如果被威胁了『想要不说出去就老老实实的听话』的话，我说不定就没办法反抗罗？」
（这句我看不懂问翻译群裡的大神 结果他们回我这段怎麼看都觉得这女孩是抖M 而且还色色的2333333333）
「不不不，那对我来说是不行的吧？」
「嗯？为什麼？弟弟君会威胁我吗？」
「不，那是不会的。不过不管在怎麼说在说有一点危机感会――」
「――你看吧？米楔尔也听到了吧？」
我的劝说连讲都还没讲完，克莱尔薇蒂娜便满面笑容的走向米楔尔。
「的确，利昂大人似乎是个有信用的人。但是，大小姐，在稍为有点警戒心吧，若發生了什麼事就太晚了喔？」
「所以说米楔尔太担心了啦，呐，弟弟君也这麼认为吧？」
「不，也稍微怀疑我一点比较好吧！?」
「欸~为了来路不明的女孩而离开管束区的弟弟君这样说了吗？」
「唔…被你这麼一说……话说回来，弟弟君？」
事到如今才在这个节骨眼上吐槽。
「你和我有血缘关系吧？」
「确实是一半相连着……」
「是阿，所以我是你的姐姐，而你是我的弟弟」
「不…这个理由我是明白的，但传到卡罗琳女士的耳裡怎麼办？」
虽然只要与我关联事情就会变的很糟糕，但是与弟弟接触曝光的话绝对会發生更不好的事情，然而，弟弟君什麼的真的没问题吗？我如此真心的感到忧虑。
「没事没事。别看我这样，我对看人可很有信心喔。」
「要是周围没有半个可以确实相信的人，步入社会之後会吃苦头喔。」
克莱尔也才7岁，还尚未建立社交圈吧，四周的人们都是与格鲁西斯家关联的人吧。我是这样想的，但是在有了社交圈之後，在四周没有友方的情况下，能够选择可以信赖的人是不太可能的吧
（求大神 後面不会 乱翻的：周りに居るのはグランシェス家に所縁のある人间ばかり。そりゃ周りに味方しかいない状况で、信頼できる人を选んでたら外れるはずがない――と、思ったのだけど、）
「呜，不会有这种事喔。因为我相信了谁除了米楔尔你还是第一个。」
「……是这样吗？」
「周围的人们，都把只我当成可以利用的道具。」
「是…是吗！?」
向米楔尔去确认的目光，她小小的点点头。
「那是……怎麼说呢，相当沉重的环境阿」
「对吧？我最讨厌了！这期间还擅自决定相亲的对像」
啊，那时所见到的哭泣的容颜是因为这个理由吗？
精神年龄超过20岁的我也成了绝望的心情阿。七岁的女孩子的话，哭了也是没办法的事吧。
「相当麻烦阿……」
「彼此彼此呢。」
……是吗？我开始感到同情，快要变成政治联姻的道具处境都是一样的吗？
「我已经明白你对我的信任，不过克莱尔薇蒂娜桑你们是怎麼来这边的阿？」
问完问题之後，克莱尔薇蒂娜突然不高兴的鼓起了脸颊。
「真是的~克莱尔薇蒂娜桑？为什麼是那麼见外的叫法？要叫克莱尔姐姐！！」
「克、克莱尔姐姐？那样真是不好意思……」
「哎~为什麼？」
怎麼说呢，其实我比你还要在年长一些，完全说不出口阿。
「欸哆…只说克莱尔姊不行吗？」
「克莱尔姊？」
「嗯。把克莱尔姐姐省略为克莱尔姊」
「嗯……不是不行，试着在说一次看看？」
「克莱尔姊」
「在说一遍！」
「克莱尔姊~?」（日文上的细微差异不会翻… 总之就是尾音提高的感觉 真的想听差别去用度娘吧）
「……嗯！我特别喜欢这样。呵呵，克莱尔姊阿~」
克莱尔姊张开了双手，跳起了舞，以咕噜咕噜的气势转动着。
虽然有着十分成熟的思维，这方面倒是显得与年龄相应的可爱。
平常都是一副逞强的感觉，或许因周遭的环境才这样提起心房，现在这样才是真正的克莱尔吧。
「那克莱尔姊来这边的有甚麼目的呢？」
「目的？那种东西什麼的没有吧？如果硬要说的话，就是想见见弟弟君吧，上次的事也还没有道谢阿。」
「道谢明明说过了阿」
「什麼阿，弟弟君不想见我吗？」
「这是……」
想好好相处的对方主动来会面，那是十分高兴的一件事。
但是…
「没关係吗？卡罗琳女士知道了会生气的吧？」
「确实。如果被知道了我和弟弟君你有关係的话是不太好，不过受到的恩情也必须要好好的报答给你。」
「…那个，算是狡猾的辩解吧」
「克莱尔大人总是这样。」
啊，总觉得可以猜想出来。
只要克莱尔姊没问题的话就好，克莱尔姊来玩的话，米莉会辛苦点不过不会在忧虑了……不会在忧虑了吧？
稍微不安的微微转头过去看米莉的感想。前後摆动的点点头了，大致上没问题吗？我还没有相处到可以判别米莉的内心。
不明白，总想跟克莱尔姊说更多更多的话，应该是之前都只能跟米莉谈天的关係吧。
「能来探访真是谢谢你阿，克莱尔姊。」
下定决心而作出感谢的答覆，原先渗漏着不安的克莱尔的脸一口气闪耀着笑颜。
「太好了。那麼弟弟君，有什麼问题想问吗？弟弟君想知道的事，我什麼都会告诉你的。」
「哎！真的？那麼能教我关於这个世界植物的知识吗？」（翻译君：这麼好的机会与时机你居然问这种问题！?希望男主向别走木头之路…）
「……植物？好是好，但是……你想知道什麼植物？」
「对了，比如――」
就这样，两人寂静而不变的世界裡，被抛掷了名为克莱尔的石头。
从中诞生的波纹，不久就变成巨大的波浪，引起各式各样的事件……这时候的我，还完全不知情。
