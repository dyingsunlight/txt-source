
商人有一辆马车，於是两人坐在马车上向精灵想去的城市出發。此时草还不怎麼茂盛，马车在辽圹的草原上驰骋着，故事也很顺利地进行着(物語ではよくありそうな、なにもない草原を走っていた。这句就算有大神指点还是不太清楚……)自我将那名圣精灵介绍进车里後她就没讲过一句话……这种(尴尬的)局面是我不擅长应付的。虽然想说些什麼，但因为还对这世界一无所知，所以对接下来会發生什麼似乎我是完全不知道的。

“……那个……”

“……怎麼了？”

“现，现在我们是要去哪裡？”

“总之向那精灵所说的城市走就对了。”

“问了是哪个国家的城市了吗？”

“嗯？你的意思是说那精灵不知道喽？”

“是，是这样吗……”

亚里亚向我投去有点不安的目光。“嘛你究竟要去哪”这样的心情我是明白的。

(我们)又向前行驶了一段时间，但此时太阳已经快落山了，所以不得不做好和亚里亚合宿的準备。马此时正悠闲地吃着草。我发动技能召唤night walker。这是50级左右的怪物，一般5只左右成群出现，所以被称为初学者杀手。我的程度是一次召唤5只。有了他们的话，应该能防止怪物的袭击了吧(这一段机翻完全看不懂所以就按照自己的想法瞎翻了)

“这……是召唤术吗？”

“仅仅是使用了技能而已啊(怎麼了？)”

“(事情到了)如果有第三个人的话就一定会说出去的地步。”

“嗯。”

“哎……╮(╯▽╰)╭”（这裡作者应该卖了个萌，两处都一样，我只好用表情代替了）

“你到底是什麼人？”

“说一直在山林裡靠着竹篮子生活（你会信吗？）”

“……”

第二天。

姑且在等待商人（送来）早饭时我解除了对夜行者的召唤。召唤兽在被召唤12小时後会消失，同时如果主人下达“解除”（命令）时也会消失。如果使用“魔术·慧尾”的话，还可以下达攻击，飞翔，骑乘的命令。在这个世界上好像有着更细微的命令（系统。）（「マジック・テイル」直接机翻了，求解）

坐在马车上就能看见远处那宏伟的城堡了。那应该就是国境了吧……大概是这样。


“……那个……”

“怎麼了？”

“进去的话是不是需要通行证之类的？”

“应该是这样吧……”


我以“被遗弃在山林中，之後被爷爷捡起（抚养大）来说明通行证什麼的是没有的。”

“总之，如果在裡面弄到行会卡的话也会被认做国民，所以得先找到方法进去的说。”

“虽然我想到幾个进去的方法，但你有什麼主意吗亚里亚？”

“你有能进去的方法？能不能说出来听听的说？”

“破坏要塞。”

“……驳回。”

“啊？为什麼？”

“我到想听听你想如何破坏要塞。”

“那，如果使用“爆破”魔法的话……”

“不行的！那样太明目张胆了！”

“那，那如果对哪裡的骑士都使用【奥義　パラライズアロー】的话……”

“不行，别给别人添麻烦了！”

“那就使用召唤术……”

“召唤术？”

“跨过这座要塞就行了。”

“那就先这样吧……”

亚里亚發出大大的叹息。不愧是（认为）被绑架了，和（像我这样的）陌生人一道旅行一定很累吧。总之，我把破魔之弓房放进储物箱中，之後从这道具箱中取出一柄纯白色的法杖。这据说是上哪裡闪灵望多恩的s级法杖（貌似有人名この杖はシャイニングワンドというSランクの装備である。）

它拥有提高魔法攻击力mp回復量和光魔法威力的能力。那为什麼要收回退魔之弓？仅仅是因为拿着弓就得带着沉重的弓袋这个微妙的理由，所以不用在意这些事。

“天马，召唤！”

“天马？”

亚里亚很惊讶的样子但决定无视掉。拥有纯白色翅膀的白色骏马出现了。噢噢现实中的天马真的很帅的说！它原本是在一年一周的天马座活动中出现的boss。生命和攻击力都很高（攻击力脑补），因为经常逃跑再加上能力又很麻烦所以被排上了boss榜前十。

“这，这是真货吗，我以为只会在故事裡出现……”

“是，是真货。那麼，（一起）乘坐吧！”


骑着天马在空中遨游。天马（柔软的皮毛）和（迎面吹来的）风都让我感觉心情舒畅。我是连飞机都没有坐过的，所以此时感觉很开心，但亚里亚似乎对此有点心悸。

“看！要塞是那麼的小！”

“不用飞那麼高！能高过要塞就行了，飞低点啊！”

老师说我还想飞高点，但亚里亚太过害怕胸部紧贴在我身上抱着我。所以越过要塞後就一口气降低高度降落到地面。

“空中的旅行还愉快吗？”

“或许将要塞破坏是个更好的选择……”

“至于这样吗……”

眼中含着泪花的亚里亚真是个可爱的孩子……我们向附近的城镇走去。在显眼的地方解除天马（召唤）是不合适的（机翻是合适，但感觉说不通），所以就在这裡解除（召唤）了。

视点变更 蕾→卢布拉

“那我走了啊！”

“好的团长！誓死守护奥卢安娜！”

“嗯，要的就是这种气势！”

我现在在奥卢安娜王国边境城市鲁透。今天我部队的任务是调查海纳教国和阿尔内森林及周边地区。

阿尔内森林，哪怕在战争结束100年的现在仍被称作魔之森林理由就是裡面存在大量超乎想象等级的魔物。正是这个原因，这片森林如今也没有属於任何一个国家的部队进去过。我们也没有进去的打算，仅仅是对周边地区是否有魔物出没进行调查而已。

“团长！卢布拉团长！”

“怎麼了？”

“一只怪兽跨过了王国的要塞！”

“什……什麼！为什麼没有阻止它？”

“（因为）那，那是天马座！”

“天马座……？！”

天马座是在100年前战争中也出现过的怪物。曾边飞在空中边给冒险者和勇士们降下雷电带来很大的痛苦。这样的怪物（要）入王都？

“如果（就这麼）让它进入王都的话会造成很大问题的！（立刻）集结骑士準备讨伐天马座！”

绝对，（要守住）王都！




