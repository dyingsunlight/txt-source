

现在拿着的枪是“神炎之枪”
炎属性的枪是最强的S等级的枪。
确实，大羊角应当是不擅长对付炎属性的，所以使用了这件装备战鬥。
顺带一提，正刺向“平原之主”的头的是“永恒之枪”。
光属性的这个枪也是S等级的装备。

“也就是说，我想设法回收那把枪”
“是这麼高价的装备的话认真地对待啊！”

黑髮男子避开“平原之主”突进的同时，一边向我吐槽。
……我的心声听到了没啊。
这样想着而發出了声音似的。

“有什麼不好！明明是我的武器！”
“虽然是这样没错，但是正如用法所说使用了的话，後面的事也考虑一下啊！”
“话、话说回来，蕾桑、盖伊！打算如何取胜啊！？”

观察到我和男子怒目而对的坏气氛了吗，茶发少女从後面说道。
啊，这个男子叫盖伊吗。
想着这些的同时我从盖伊的视野离开了，面向少女那边。

“嗯…总之我想回收那把枪”
“蕾桑有想打倒‘平原之主’的打算吧？这样的话打倒之後再回收枪，这样的话……”
“不行，因为没有那把枪使用不了这麼强威力的攻击”

在「Magic·Tail」攻击会损耗装备……为什麼这裡没有呢，不过在这边的世界这种事也考虑上比较好吧。<蕾竟然会考虑这个了，感动！>
毕竟制作永恒之枪的人现在哪裡也不在。

“是、是这样吗……”
“所以，我要想办法回收枪……你有什麼办法吗？”
“不不！蕾！很奇怪啊！”

我说了意见後，盖伊马上就反驳呢~。

“怎麼了？不满？”
“不满也是其他也是都很奇怪啊！能使用这麼厲害的攻击的话一开始就使用啊！”<终於有人吐槽蕾这个都比了>
“嘛，虽然是正确的言论。但我这边也有各种必要的準备啊……心理什麼的”<心理準备…>
我说完自己的意见时，“平原之主”的角痛快地往这边挥舞。
我们各用各的方法避开了那个角。
暂且不说我，那两人说不定也意外的强。

“好了！那麼我去取回我的枪了，所以……加油”
“欸……喂！等等！”

无视掉盖伊正说着的什麼，发动【召唤 始祖神鸟】，出现後马上就一跃而上。
之後，急速上升。
一口气去到比“平原之主”还要高的高度。

“那麼，怎麼取回呢？”

做好拿枪的姿势的同时在“平原之主”的上方回旋苦恼着。


视点变更 蕾→娜鲁

“啊，真是的！怎麼回事啊！那傢伙！”

蕾桑飞走后，盖伊大声地抱怨道。

“冷静点盖伊。姑且，蕾桑也协力我们，所以……”
“所以说是怎麼回事啊！—那个态度！比起大量的人命，一根枪更加重要吗！？”

盖伊非常认真的生气了。
总是温柔可靠的盖伊，一旦与人命有关的话，就会变得这个样子失去冷静。
……嘛，作为人类是个不错的人，但作为冒险者稍微有点那个……

“是爱着自己持有的武器的人啊”
“如果是这样的话会把枪投出去吗！？”

盖伊完全上了头似的，我的话完全听不进去。

“但是现在听蕾桑的话想办法拖延时间吧！”
“……没办法”

虽然盖伊没有相信蕾桑的事，但是似乎在想办法理解。
我呼地叹了口气，沉思着不经意产生的疑问。

“说起来，‘平原之主’没有袭击过来呢”
“是、是啊……确实，那傢伙是意外冷静地行动的怪物，但是什麼都不做也太过冷静了。”

没错，从刚才开始“平原之主”就完全不动。
姑且，和盖伊对话时也警戒着，但是从刚才开始就一直不动。
我做着持武器的姿势的同时，好好的开始观察起“平原之主”。
……这是

“……线？”
“什麼，怎麼了？娜鲁”
“盖伊，‘平原之主’的身体被线缠绕住了”

是线，在日光的照射下才能看到的这种程度的细线在“平原之主”的身体中缠绕着。

“确实……但是这样的线使‘平原之主’变的动弹不了，怎麼可……”
“是呢……”

那个线正体不明而可疑，但是这种线的程度应该不能停止“平原之主”的动作才对。
这样思考着时，从“平原之主”头部的附近听到声音。

“喂~！要拔枪了哦~！”

是蕾桑的声音。
而且直接乘着“平原之主”。

“……，欸？”

直接乘着头？我对蕾桑不怕死的行动而脑中变得一片空白。
之後，简直就像在空中散步一样缓慢地往这边步行过来。

“咿呀~抱歉抱歉，明明停止了‘平原之主’的行动，却感到困惑”
“欸，呐，你在做什麼？”

我情不自禁地问道。
不如说，我和盖伊在说话期间做了什麼？
说起来，不久之前才载满吐槽，但是现在只想问这是做什麼。

“啊啊，没什麼大不了的哦。对‘平原之主’从空中使用【奥义 麻痹撞击】使其停止行动，陷阱蜘蛛们只是咕噜咕噜地给其缠卷住。”

“似乎动不了呢”这样补充道的同时向我解说。

“……”
“……”
“欸？什麼？2人同时发呆了”

可爱的歪着头的蕾桑。
但是这个人幾乎一个人使那个“平原之主”显现无力化。

“无力化这个知道了，但是现在打算怎麼办啊！”

虽然是和我一起呆掉的盖伊，终於回到现实似的向蕾桑抱怨道。

“嗯？打算现在开始攻击哦？毕竟停止其动作是为了去取回枪”
“怎样的攻击？”
“这样的”

说了一句话後蕾桑手拿着赤红的枪开始一圈圈的旋转。
於是，火从枪的前端喷射出来，渐渐地火轮出现了。

“【奥义 长矛飞镖】”

蕾桑这样说了後，回旋着的枪尽情地向“平原之主”投去。
枪描绘出一个红圆的同时向“平原之主”的所在飞去。
接着，枪命中“平原之主”身体的瞬间

“咕啊啊啊啊啊啊!”

“平原之主”的身体一瞬间变成火块。

“嗯，这样就结束了。”
“……”

看着蕾桑那像是完成了什麼大事件似的神清气爽的表情，我和盖伊就这样呆然着。

“好了，那你们两人一起回去吧。你的同伴不是已经在中转站了吗？啊，但是也可能逃跑了……”

注意到我们的样子，蕾桑不停地说道。
我感觉和这个人有着巨大的代沟。
我虽然认为自己有弓的才能，但是看到压倒性力量和自己同年的她後，失去了自信。

“蕾桑，是什麼人？”
“嗯？只是闭门不出的精灵哦”




