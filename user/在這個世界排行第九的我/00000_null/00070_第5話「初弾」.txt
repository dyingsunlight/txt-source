自到达哈伦斯已过数日。
自己也终於开始习惯起新环境。
然後今天，接受哈伦斯国立魔法学园的入学考试的日子终於到来。
我现在正在前往学园的路上。

[好想快点被解放……]

一心想着快点结束。
已经受够学习了，而且就这麼走着走着都感觉公式啊文字啊歷史啊，这些记住的东西都要挥发了一般。
学园的位置正处在贵族区和平民区的间隙中。
从地理上说的话就在这个都市的中心附近。

（话说果然被周围的人注视了啊……）

果然银髮或者眼睛很引人注目吧。
幾天前出去买东西也承受过和现在一样的视线。
我专心堂堂正正前行。
毕竟被赛萝拉小姐彻底地吩咐过第一印象是很重要的。

（周围也是看起来很多同龄，估计也有和我一样的考生吧。）

前方不远处的少女低头走着。
估计是在读着教科书吧，正所谓最後的冲刺。

（话说，掉了什麼东西吧？）

这位处在最後冲刺紧要关头的少女。
埋头学习不顾其他，而弄掉了带着细绳的小小东西。
那个是护身符吗？感觉会是为了祈愿合格一类的东西吧。
但她并未注意到，而就那样笔直前行。

（只能由我来做了吗……）

因为迎面见到了现场，就这样不管不顾走过去事後也会不舒服。
这就算练习不过练习而已啦，就进行和同龄的异性交谈的练习吧。
毕竟至今为止都只和年龄比我大的异性对话过，而且还没进行过正经的交流。
捡起失物，下定决心靠近过去，然後搭话吧。
没问题的，我也并不是要做什麼奇怪的事。
不如说这是善举，是应当被褒奖的行为啊。

[那个，打扰一下。]
[好的————]

顺利的搭话了，如同预料她转了过来。
曾经接受过与他人对话的时候应当笔直看向对方眼睛，如果无法做到就看向双眼之间，或者鼻子的教诲。
进行实践，而这样进行对峙的时候，不知为何她半路呆住了。
仿若被冰冻住了一般，嘴大大敞开，瞳孔也扩张开来。
虽然乍一看像是被吓了一跳，但我可不记得自己做过什麼奇怪的事情啊。

[那个……]
[呀、呀噫！]

（咬舌头咬的很厲害啊，是那种吗？这一位也不擅长对话吗？）

但是外表看起来完全就是都市女孩。
在哈伦斯中的仅次於金髮的发色就是茶发，这个人也是茶发，所以才将她当作城裡的女孩的。
外表也好打扮也好都温文尔雅的，实在不觉得和我是同类。

[这个，你弄掉了哦。]
[诶？]
[因为你没注意到。]
[十、十十、十分感谢！]

总之先把捡到的东西交给她吧。
话说回来为什麼是这种战战兢兢的反应呢。
难道是我看起来是很不妙的傢伙什麼的吗？还是说因为我的头髮或者瞳孔是银色的，或者乾脆就是脸不行呢。
果然boss他们说我养眼完全就是客道话。
正以为眼前的人吓呆了的时候又红着脸低下了头。

（难道说是生气了吗……？乡下人别靠近过来啊，那种意思的……？）

但是如果说和我的外表或者态度都没有关係的话，能够想到的就是智慧热一类东西了吧。
也即是学习过度，嗯就把这当作脸红的原因吧。
这裡应当向着积极的方向思考。
嘛总之已经将失物好好的交过去了，已经再没有留在这裡的意义了吧。

[那、那麼别过。]
[唉……]

一副不是还有别的事情吗的表情，已经结束了，已经要撤退了。
继续对话也很尴尬。
唯有迅速撤离此处方可。

（不过意外的是我普通的对话了哦？都觉得凭心而为都能行得通了。）


虽然总觉得对方反应很奇怪，但还是说出想说的话了。
曾经对话也只是三言两语的自己也能主动发起对话了。
仅仅这样就足够了。

[好的，入学考试也这样勇往直前吧。]

幹劲十足，动力充分。
之後就只剩出结果而已。
