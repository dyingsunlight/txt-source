32
在黑夜之中挥舞着红色剑刃。

来者何人？

优人低声的询问着。
伴随着疯狂般的吼声，黑骑士行动了。
持续警戒状态的优人抽出剑的瞬间，停止叫声的黑骑士如瞬间移动般攻到眼前向着优人挥舞出剑刃。

“闭嘴，人类。”

冷到让人背脊发凉的声音。
激烈的金属碰撞音，响彻在静寂的广场中。
用屠龙剑接住黑骑士斩击的优人，就算是完全防御住，也被向後方击退。

“夏奈！唯，退下！”

我一边提醒夏奈对优人进行援护，一边向着带然站立不动的唯呼喊。
怎麼看唯都是非战鬥人员。不论银气如何提高身体能力，卷入这样的战鬥不可能完全无事。

为了援护优人夏奈迅速的準备好弓箭，用闪耀着银色光辉的箭發出三连射。这种距离的射击很轻易的被黑骑士的剑击落。

“什麼啊这傢伙！”

“唯姐，快逃！”
唯听到夏奈呼喊的唯，晃动凌乱的长髮向我方跑来
黑骑士将攻击目标改成夏奈，下压剑首快速接近目标。

已经判断无法施展远攻的夏奈，只好放下弓拔出腰间的短剑。就在夏奈抽出武器的瞬间，优人的大剑已经向着黑骑士横切过来。

，黑骑士不得不单手接住突然来到的斩击，利用这个空隙重新搭上箭的夏奈继续援护攻击，但依旧被防御住无法造成伤害。

我慌乱的从通用门向内城撤退

“敌袭！集合警备！叫醒カリスト还有...”一瞬间的犹豫，
“联络西里斯，一定要要喊起来，非常事态！”

那些不论说什麼都没有反应呆立的守卫，在我 “快一点！” 的催促下才各自的行动起来，宣告非常事态的钟声也随即鸣响。

我从警备所取出保管的佩剑挂在裙子上，并且手握长枪，重新返回到城门前的广场。

通用门附近，唯抱着包裹，担心着优人与黑骑士的攻防战，目不转睛的看着我把枪夹在腋下坚持着，如果一度停止的话，会因为恐怖而无法行动。

优人银色的大剑一击挥空，击碎了地板。就在这个硬直的时间裡，追击援护的夏奈放出的箭羽，全部被击落了。

优人他们那样银气持有者的战鬥，我是无法加入的。但不论用什麼方法都想援护他们。

不能错过任何机会。


优人因为强烈的连续攻击不停地後退。望着正在兴奋的用红色剑刃进攻防御中的优人的黑骑士，瞄准他的侧头部，我全力的转身用枪的金属部分攻击过去。

撞击声响起。感觉好像打在了岩石上。虽然枪柄没有折损，但也因为这反冲击力而脱手而出。我没有去拾取它而是抽出剑来继续进攻他的侧腹。

砍击盔甲的剑，传回刚才同样强烈的反冲击力。

手部传来的麻木感让我皱起眉头。望向剑锋，钢刃已经出现缺口。

但即使是一瞬间的失神，也是足以致命的。

压制着利用银气提高体能的优人的黑骑士，在我愣神的瞬间，出现在眼前。

红色细长的眼睛。

好像在嘲笑着。这个傢伙...！

背部涌起恶寒。

我猛然後退，黑骑士更快一步的伸出手来。

撒手丢掉红色剑刃，空手将我的头抓住。

呀....啊！

头部传来的激烈疼痛让我丢掉了剑。并且全力的想要从黑甲的手腕中挣脱出来，但抓住头部的手纹丝不动。

不顾及裙子的蓬乱不停地用腿踢踹黑骑士，当然对於这样的对手根本没用

“你，我见过”
黑骑士歪这头说道

「カナデ！」
「カナデちゃん！」
优人和夏奈同时喊道。优人斩向黑骑士的背部，夏奈射出缠绕着银光的箭。

我被粗暴的抡起丢向空中。
一瞬间仿佛感受到了永远的漂浮感後，我，强烈的撞击地面。

彭!
被抛出後的力度没有停止，咕噜咕噜的在石板上继续滚动，眼角的余光看到再次和黑骑士砍杀到一起的优人。

唔！

咬着牙忍受着强烈的疼痛感，努力了爬起身来。
向着优人和黑骑士方向的反对面，大街的方向跑去。

胸中好痛。
并不只是因为和地面的撞击。
咬唇忍住心中的不甘。
优人和夏奈明明在那样的努力着，我却什麼都做不到。
可恶..

难道就没有我能够做到的事情了麼？
这个时候，城门打开了。
城墙上出现的弓兵们并排站立，拉开弓弦。城塞设置的巨大弓弩缓慢的转动，并且瞄准了广场的中心。

城壁中穿戴盔甲带着枪和剑的骑士和士兵们倾巢而出，将和优人激战的黑骑士远远的包围住。
穿着轻甲的カリスト和穿着便装的西里斯走到最前列。
“那裡的黑甲人，马上丢掉武器！”
　カリスト喊道。
黑骑士弹开优人的剑，抬腿踢飞站姿不稳的优人，然後悠闲的看着周围围拢他的骑士们。
起初，站在这裡的大家谁也听不出对方發出的声音是何种意思。
金属和金属摩擦产生的刺耳高音，真的是让人非常不快的声音。

“咔咔咔咔咔咔咔”

声音是黑骑士發出的，黑色的铠甲小幅度的震动着。现在大家才发觉到刚才的声响是黑骑士的笑声。

咔咔咔咔咔咔咔！

剑无力的垂下来，黑骑士咕噜咕噜的转动着头部。

“增加了，增加了。。。垃圾越来越多了！”
令人毛骨悚然的气息，让周围的人不仅向後退去。 但这其中有一个挥动着巨剑的身影。

优人闪烁银色光辉的大剑斩下。

我无视身体的疼痛喊道：

“カリスト！大概只有银气才有效！西里斯，请援护优人！”
这时候二人才注意到蹲伏在黑暗中的我。望着破破烂烂的我，两人吃惊的睁大双眼！
大概是察觉到我的用意，西里斯行动了。
我的剑完全无效的事。优人才能有效给予伤害。从这裡推断出大概只有银气才能伤害到那个傢伙。

没错，就像魔兽一样。

优人後退的瞬间，代替他进攻的西里斯突然插入，干脆利落的一剑。黑骑士用手甲从容的挡住。

而西里斯就这样冲过来。
向着我的方向。

哎？

夏奈的援护，按照我的指示手中挥舞着银制武器的骑士们和优人一起激烈的攻防战。根本无心关注这些的西里斯直直的向我奔来。

“笨蛋么，你这傢伙！”
“什麼..”
被突然地怒吼震惊的我不知道如何回答。
“又弄成这破破烂烂的模样，考虑一下自己的安全啊！前阵杀敌并不是指挥官的工作！”

呃,确实是这样。
并不只是现在，不论什麼情况都是...

无视低下头的我，西里斯呼的一下抱起我。

“什麼，停止，放我下来！”

似乎根本听不到我的抗议声，西里斯就这样的走起来。

绕过战鬥的广场中心，来到了カリスト所在的地方，然後轻轻的放下了我。
对於突然發生的这件事，我只是呆呆的任由摆布。

「カナデちゃん、没事吧？」
唯慌忙的过来快速的释放起治疗术，瞬间被唯的魔法包围。
　「カナデ。你的预测应该是正确的。」
西里斯眺望着黑骑士那边的战鬥。
　“看，那个黑骑士一直在回避着的只有优人和夏奈的攻击。对周围骑士的攻击根本置之不理。也就是说，优人他们那裡才能够给他造成伤害的可能性很高。”
西里斯皱眉说道。
“那个少年还不能很好的操纵大剑，单轮剑技的话，カナデ你要更胜一筹，他不能灵活运用是无法产生大的威力。”
西里斯举起了剑，目不转睛的看着我。从他微笑的目光中感受到了温柔。
“我也去援护他们，你就安安静静的在这裡等我。”
呃..
果然没有我能够做的事呀？
西里斯举剑突击过去。
“配合进攻！少年！：
“啊，是你！”
西里斯攻击结束的瞬间优人顺势进攻。但仅仅利用单手的黑骑士还是能够从容的化解攻势。

如果能够组织那只脚的动作的话...
我想我可以的。

「カナデ大人。您没事吧？」
　カリスト担心的看着我。
「カナデ大人…」
「公主殿下！」
「请您不要乱来！」
「没受伤吧ー！」
等候在一旁的骑士团的各位都在关心的询问着。
　就算是为了大家.我也要..
忽然想到了什麼。
　大家，我虽然是没有银气的才能的。
　但正因为如此才更要去帮助他们。
并不是单纯的一个人去意气用事。
而是要集合大家的力量。
　这样的话，也许会帮上忙...？
碰碰运气吧。
　安慰一下为我治疗的唯，我站起身来。治疗术并没有立即生效的特性，但多少感觉身体恢復些了。
”夏奈,快来！カリスト，城堡的弩炮，还可以使用吧？“

カリスト点头。
”可以马上準备好火焰箭么？“
”明白！“
”在西侧的弩炮配备火焰箭和最好的射手。“
カリスト一边点着头一边去传令。
夏奈沉着脸回来了。
“箭都要用完了，什麼啊那傢伙，真是不敢相信！”
”夏奈，现在马上登上城壁，操控东侧的弩炮，将银气注入射击用的大枪中，然後等待与我一起合击“
　就算并不是可以使用银气的勇者装备，拥有强大力量的夏奈利用自身的力量的话，应该也会对黑骑士形成很大威胁。如果再算上弓弩射出的大枪的质量和运动能量，很有可能会对黑骑士造成有效伤害吧。
但是夏奈露出诧异的表情。
”在夜裡又是黑色的铠甲，还没有绝对能打中的自信，弩炮什麼的也不太了解...“
”没问题的“
我笑着说。
”カリスト，人选就拜托你了，能够帮助夏奈的，对弓弩操纵熟练的士兵。“
我继续盯紧黑骑士。
”作为制造混乱元凶的黑骑士，要在这裡制服住他。大家，借给我力量吧！”
回应我的声音，响彻天空。
　优人和西里斯交互攻击，优人攻击下方，西里斯就攻击上方，利用盔甲弹开骑士们的攻击的黑骑士，面对多人的围攻，依然能完全支配战场的节奏。

　「カナデ大人」
カリスト报告说一切準备完毕。
我点下头。
要开始了！
为了不错过重要的时机，城门中配备了可以传达我的意图的手旗手。
　西面的弩炮随时待命。
我斟酌着时机。
优人後退中，西里斯斩击，然後再西里斯躲闪黑骑士大幅度攻击得瞬间。
就是现在！
“西里斯，优人退下！”
　我向手旗兵發出讯号。
　听到我的警告两人飞速撤离。
　瞬间。
　犹如射出的箭一样，带着切开风的巨声，大枪击中了黑骑士的附近。地板發出碎裂的声音，大枪前段装配的火焰瓶炸开形成了大範围的火攻。
没有直接命中，虽然夜裡并不好瞄准但很好的射在了离敌人最近的地方。
火焰大枪击中地面的同时，油四溅到周围，黑骑士立即被火焰包围。

“咔咔咔咔”

站在火中的黑骑士在笑。
当然没有依靠火攻就能取胜那样乐观的想法。
但因为火焰的照射，就算在夜裡也能清晰的看到黑骑士了。
没有丝毫耽搁我向夏奈發出信号。
看你的了，夏奈！
再起响起将空气撕裂的声音。
能够完全看清敌人的身姿，值得骄傲的白燐騎士団的射手们和夏奈不会错过这个机会，也不能错过。
参杂着夏奈银气的大枪，还是比被火焰包围的黑骑士的反应慢了一拍，果然丢掉手中剑的黑骑士，单手就抓住了长枪。但相应的，黑骑士被飞来的大枪的重量影响，一瞬间出现了无法行动的时机。
　是了！只要这一刻就好！　
　注意到这点，我跨前一步，
　“优人，现在！”
用尽全部的力气，幾乎达到了声音的极限。
“少年！”　
“优人！”
“优人桑！”
西里斯，唯，カリスト也同时喊道。
好像回应大家一般，屠戮大剑并发出大量的银光形成更加巨大的剑。
我们所有人中，最大最强的火力的一击。
“喔喔喔喔喔喔！”
银色巨刃，向着被弓弩打枪固定不动的黑骑士斩去。
金属的爆裂声。
黑色的铠甲被砍飞。
　优人银气炸裂放出的光芒让广场一瞬间犹如白天。
然後，满满的光芒消失了。
那裡站立的，是保持着攻击姿态的优人，和从肩口到胸部被完全斩裂的黑骑士。
成功了吧？
四周寂静。就好像什麼都没曾發生过一样。
　但是，这种安宁，被难听的异音打破了。
　”咔咔咔咔咔“
究竟是何意不能马上明白，但就像收音机的噪音渐渐的被调谐一般，汇集成一个单词。
“咔咔，杀，杀，杀杀杀杀杀杀杀杀光..！”

　半个身体被砍裂的黑骑士，头盔破碎的同时發出强烈的红色光芒。
