和帝国战鬥（的日子）将近，关於情报收集的優先次序相当的高。

这回的战鬥的必要条件，是至到帝国士兵抵达补给基地为止，彻底地阻碍他们的脚步这件事。为了这个（达成目标），（要在）帝国的士兵出發後马上，不对，（要在）出發之前的阶段，咬人猫就出阵是必要的。

为了掌握敌人的行动，咬人猫的成员（member）以四人一组，五天轮班一次，进行着侦查任务（而向帝国出發了）。

「罗尔你们抽到不好的签了。真不好意思」
「没有那样的事。嘛~，老实说，在吃枫糖浆（maple syrup）之前就出發了真是遗憾，那我回来後会让我吃的吧」

对着要进行侦查任务而从早晨就得出發的罗尔他们的四人说出了谢罪的话。
他们想在出發之前能够吃上枫糖浆。
但是，（任务）目的地是靠近帝国的补给基地而制作的小屋，即便是一起用了风魔术和【身体強化】的罗尔他们，这也是100km以上的距离，如果不在早晨出發的话太阳下山之前抵达不了（目的地）。

「会好好準备枫糖浆的哦。在罗尔你们回来的时候酒也已经酿好了，好好的期待吧」
「这真是让人期待。村长，那麼（我们）出發了。要是轮班一直不来的话，现在出去了的雷克（レック）他们就太可怜了」（译：西利鲁的青梅竹马男）

然後，罗尔他们四人就出發了。

背着我所制作的连接着腰带（hip belt）的背包（ruck），穿着防寒衣物就出發了。
（译：虽然看过其他的地方翻ruck为登山包，基於时代背景，我翻成了背包）

背包裡面，有一周份的粮食和替换衣物，以及关入了我所养育的五只鸽子的鸟笼。

同时也兼当训练，他们在侦查任务的期间的粮食只有软饼乾和自力在森林得到的东西。
（不完整，原文：訓練も兼ねており、彼らは偵察任務の間は糧食のクッキーと森で自力で取ったものしか口にしない）
虽然侦查任务本身并没有要求要完成特别困难的事，然而却是会对精神造成痛苦的任务。

「帝国至今还没有行动吗？」

在送走罗尔他们之前，我在他们的面前，看了从之前执行侦查任务的傢伙用鸽子传来的信的内容。

在上面写着的是，进入补给基地和从补给基地出去的马车数量，这个是以帝国方面，和艾露西艾方面分别写的（语死早，原文：それらが帝国方面と、エルシエ方面にわけて書かれてある）。

「如预想那样，更正式的搬运还要等上一阵子」
（脑补，原文：本格的な、荷運びはもう少し後になると予想した通りだな）

我在距离补给基地500m的山裡有一个，在补给基地和帝国之间有三个，去了制造几套保存粮食和备用的箭矢的小屋和水井。
在（制作）那个之际，我潜入了补给基地确认过了但是，五千人份的粮食和武器都没有準备好。

也就是说，要开始那些傢伙的战鬥，有增加补给基地的储备的必要。
（本来就）有一定数量的马车会在定期内进入补给基地，除此以外的马车的数量增加了的话就是战鬥的征兆了。变成那样的话就会进行更深入的侦查，在情报的准确度提高後就出阵。
（最後一句不确定，原文：情報の確度を高めてから出陣する）

咬人猫的各位，从距离补给基地很近的小屋，使用望远镜，一整天都在监视，得知了马车的数量。（最後一句不完整，原文：馬車の数をカウントしてもらっているのだ）
从每日的变化来推测，现在还没问题。
（意译，原文：毎日推移を見る限り、まだ大丈夫そうだ）

「还没有来吗」

我所养育的鸽子在脚上被绑着信回来了。
鸽子它，是从执行侦查任务的队员放回来的。
（不完整，原文：鳩は、夜明けと共に偵察任務についているイラクサの隊員から放たれる）

也就是所谓的信鸽（原文：伝書鳩）。在没有无线通信的这个时代，是被想出来的为数个最有效率的通信手段之一。
难点在於只能进行单方通信这一点。

虽然很容易被误会，鸽子们是不会向着我们所预期的目的地前进的。它们只会有凭着归巢的本能回到会到自己的巢的行动罢了。
（意译，原文：勘違いされやすいが、鳩たちはこちらの意図した目的地に向かってくれるわけではない。帰巣本能により自らの巣に帰ってくるという動きをするだけである）
（缺点有两个，）如果用完了预先带过去侦查地的鸽子（讯息的传达）就结束了，也做不到从艾露西艾向侦察队发送情报的事。
（脑补，原文：あらかじめ偵察地に持っていった鳩を使いきればそれで終わりだし、エルシエから偵察隊へ情報を送ることはできない）

正因为如此，有每五天就让交替人员把飞回来的鸽子带到（侦查地）的必要。

然後，咬人猫有个规定，要是连续两天在鸽子能飞行的情况下没有鸽子回来的话，会判断为遇上了麻烦（trouble）而会送出救援。


「撒～，我也开始我的工作吧」
（脑补，さて、俺は俺の仕事をするか）

今天是枫糖浆的收获日。
大家，都衷心期待那非常非常甜的糖浆的吧。

◇

「大家，把每一个水瓶加热吧」

日前，我和火狐们比妖精更早来到了设置了水瓶的地方。
首先，不準备空的水瓶的话，就做不了从别的树採集的準备，（所以）有必要拜托先行出發的火狐们工作。

「我知道了。希利路君。让它沸腾是不行的呢」
「真不愧是，库舞。好好的记住了呢。大家也一样在不让它沸腾的情况下把水份蒸发掉。一直把水份给蒸发掉至到变成浅金色。要是觉得做好了的就叫我一声。我会来检查（check）的」

火狐们对於我的话点了点头，开始了把树液变成枫糖浆的作业。
在这个场合就处理是有理由的。

除了有一百四十个个水瓶以外，由於一个水瓶内装有200L的树液所以非常的重。把这些带回到艾露西艾，在别的场所转移，然後再把空了的水瓶运回来的话是非常需要劳力的。
（意译，原文：水瓶は百四十個ある上に、樹液は一つの水瓶に200Lほど入っており非常に重い。それをもってエルシエに戻り中身を別の場所にうつしてから、空になった水瓶を持ってくるのは非常に労力がかかってしまう）

正因为如此，借助了火狐们的力量，在这个场合把水瓶内的内容全部变成糖浆。这样一来，可以把内容减少到四十分之一。

然後，（我们）把到今天的收获日为止的五天裡又追加準备的五个水瓶给带来了。
在这个场合（把树液）煮沸，（然後）把那个内容往（新的）五个水瓶转移，既有的水瓶可以变空，把区区五个水瓶运回去就能够结束了。
（译：我看回去才发现，什麼水瓶啊，明明就水桶好不好，200L啊！）

「黑音的水瓶、变成了漂亮的金色了の」
「我这边做的这个、才漂亮哦」
「黑音也好柯敏也好、还是太天真了。你们两人一次煮两个、雪乃呢、则是一次煮三个。所以是最厲害的」

小妹妹的三人在享受的情况下持续着作业。
她们三人即便在火狐族之中也是持有着相当强大的力量，大多数的火狐们即便只是煮一个就已经拼全力了，黑音和柯敏则是一次煮两个，至于雪乃更是可以一次煮三个，把水份给蒸发掉。

比起纯粹的力量，不如说魔术的制御的灵巧度高的令人咋舌。
虽然如此，库舞却是轻鬆地一边哼着歌曲，一边一次煮着四个的完成工作。
（脑补，原文：もっとも、その横でクウは鼻歌交じりに四つ一度にこなしていたりするのだが）

「撒~大家，在妖精过来之前就先结束，给他们一个惊喜吧」
「希利路君，我会加油的」
「如果是为了希利路哥哥大人的话会加油的」

香甜，幸福的味道在四周飘散。
一定，会以最棒的味道被完成的吧。

◇

从火狐们到达（枫树区）开始算起一个小时後，妖精们到了。
和上一会不一样，不单只是对自力的体力有信心的人，艾露西艾基本上全部的人都来了。由於有枫糖浆的试吃会，所以我就这麼安排了。

在妖精们到达之际，把树液变成枫糖浆的加工结束了，把它们转移至带来的追加（制造）的水瓶的（工作）也结束了。採集到的枫糖浆，得到了750L，是预想以上的（收获）。

（我让）疲劳于（加工）枫糖浆的作业的火狐们去休息，而妖精们则逐个逐个把软管换去别的树，（为了）让变空了的水瓶（再一次）装满树液而行动了起来。

如果附近没有刚好适合的树的话，不搬动水瓶是不行的，这个是在这裡的重活。

「希利路，这一边（的作业）结束了哦」
「村长，这一边也OK了」

从指挥妖精们的露西艾和空娜得到了全部行程完成了的联络。
那麼一来，今天的作业就（全部）结束了。再五天後，又要进行同样的作业了吧。

◇

「大家，辛苦了。多亏大家的努力，（我们）得到了很多的枫糖浆哦。这样一来，就能充分的确保要卖出去（的量）。然後，我们能够享受的份也好好的留下来了」

我的话让大家笑了。
上一回，说过了【会先優先贩卖的量，要是採取的量很少的话，我们能够吃的份会没有】这样的话，他们会在意也不是不可能的。
从刚才开始闻着糖浆（散发）的香味，大家都兴趣十足。

「大家、都有把麵包带来吗！」
「「「是！」」」

很有气势的回答了，他们从各自的行李把麵包拿了出来。

「（这些）枫糖浆呢、是火狐和妖精（一起）协力聚集到的东西哦。虽然现在说有点恶心（脑补，原文：くさい言い回しになると），（这个是）妖精和火狐的友好的味道。希望你们能够衷心的享受。那麼，过来排队吧」

对於我的话，大家一起动了起来。

◇

（大家）在收集了枫糖浆的五个水瓶前面排了队。
在每一个水瓶前面有两个火狐负责配膳，在我眼前的水瓶则有我和库舞在。

「倪安库桑（ネンクさん）、（你的）麵包要烤吗？」

库舞对（排在队伍）前头的，（外貌）纯朴的妖精男性说道。
库舞她只要一找到时间和机会，就会和每一个一个的妖精说话，让别人认识自己（原文：顔を売っている）。然後，只要听过一次之後绝对不会把（对方的）名字给忘掉。所以，即便是这样的场合也能叫出对方的名字。能够好好的叫出别人的名字。虽然是些琐碎的事，但会（他们）对（库舞）印象会有所改善。

库舞她，由於她自己是火狐的代表，理解到了妖精们对於她的看法都同时也代表着妖精们对於火狐全体的看法，因此才这麼做的。

「啊，那个，拜托你了」
「那麼，我先帮你保管（你的麵包）」

库舞露出灿烂的笑容从妖精的手上接过了麵包。
库舞的笑容，以及（在转交麵包那瞬间）触碰到（她的）手，纯朴的妖精的脸顿时变红了。啊~，喜欢上啦。

……罗尔也好，这个男性也好，库舞可能是魔性的女人也说不定。我听到了有不少的男性对库舞有着不错的印象。

「好，希利路君，拜托你了」
「啊～，之後就交给我吧」

我从库舞那裡接过了烤地香香的麵包，（然後）充分地放满了枫糖浆。在接过这个后手肯定会粘粘的，不过会在意这种事的人在艾露西艾是没有的。

「给你（はい），请慢用（どうぞ）」
「……谢谢」

我把放满了糖浆的麵包交了出去，男性一副由心失望的脸。一定是希望能够经由库舞的手交出来的吧。一定是想过想【舔一舔沾附在库舞的手的枫糖浆】，【库舞酱的好甜好甜】之类的……（我）绝对不会容许。
（语死早，原文：クウの手に付着したメープルシロップをぺろぺろして、クウちゃんの蜜甘いよとか考えていたに決まっている……絶対に許さない）

在想着这样的事的当儿，一个一个的队列出现了。
不知道为什麼，只有我前面的队列异常的多（人），明明其他的队列已经变得没有人了的说，（只有我这裡）还完全看不见队列有消失的迹象。
（最後一句语死早，原文：まだまだ列が尽きる気配がない）

（应该是因为）有盯上库舞，和盯上我的客人的缘故吧。就像是这些孩子一样……

「希利路哥哥大人！拜托你了の。小黑，喜欢甜的。（给我）加好多好多吧」

用着天真烂漫的脸，把自己的慾望直接（straight）地说出来。黑火狐的黑音。

「黑音，不可以做卑鄙的事の。那个，我也，有着想要很多的（枫糖浆）这样的心情」

从後面搭上黑音的肩膀（原文：背伸びして），（虽然）一副大人的模样（说教着），（眼睛却）闪亮闪亮地看着枫糖浆，露出对老实说出来的黑音一脸羡慕的黄火狐的柯敏。
（语死早，原文：背伸びして大人ぶりながらも、ちらちらとメープルシロップを見たり、素直に言えるクロネを羨ましがっている黄色い火狐のケミン）

「希利路哥哥大人！」

说出来的话虽然非常的简单（simple），用着闪闪发亮的眼睛，真挚地看着我的眼睛，（表达着）自己的心情……【请给我一堆枫糖浆】表达着这样的心情的银色火狐的雪乃。

她们三人，都準备了两个麵包。一个是今天的午饭的份，然後另一个，是她们三人，把昨天的晚饭的份，偷偷摸摸的藏起来确保的，从库舞那裡听来了。
一个她们自己烤了，（另）一个就保持原样，看起来是打算用两种种类的麵包充分地享受枫糖浆。

「那麼（じゃあ）、我来準备（你们的）麵包」

在我拿着麵包的当儿，她们三人用着非常认真的表情凝视着我的手。
虽然我很想回应她们三人的期待。但是，不管是多麼可爱的妹妹也好，（我也）没有（给她们）特别待遇的打算。
已经到了，不能说是一堆的等级了，我把浸透了糖浆的麵包交给了（她们）三人。（译：说了那麼多，原来是个傲娇）

「呜哇~、好多的糖浆啊の。はむ（拟声词：一口咬下），好好吃の。小黑，最喜欢希利路哥哥大人了の！」
「好厲害，竟然这麼多！！希利路哥哥大人，谢谢你！」
「希利路哥哥大人。雪乃，一生都会跟着你」（译：来~大家都来学做糖浆吧）

她们三人脸和手上都沾满了枫糖浆的同时，尾巴要撕裂的似的摇摆着，即便是现在也想抱过来似的欢乐着。真的好可爱。比起枫糖浆，我更想把这些孩子给吃掉。（译：原来旗是在这边立的）

她们三人带着幸福的脸把麵包塞满整个嘴巴直至脸颊被撑了起来，然後离开了。
其实我是真的好想把这个水瓶都给她们的，因为还要给其他人的麵包加上（枫糖浆）所以才忍住的。

「那个，希利路君」
「我知道的。库舞也想更温柔地做的不是吗？（待遇）不好好区分是不行的呢」
「……希利路君也有这样的地方呢。反过来被安心了」
「嗯？虽然我不是很明白，但是还有人在等待中哦。闲谈就先打住吧」
「也是呢。啊～哈梅伦桑（ハメルさん）。好久不见。你的麵包要烤吗？」
（第2和3句不确定，原文：
「わかってる。クウはもっと、優しくしてやれと思っているんだろう？　けじめはきちんとしないとね」
「……シリルくんも、そういうところあるんですね。逆にほっとしました」）

然後，队列渐渐地被消化掉了。

◇

「希利路，这个真的很好吃呢」
「能够合露西艾你的胃口我也很高兴」

总算完成了全员的枫糖浆的供应，我和露西艾两个人，靠在和大家有点距离的树上，吃着我们自己的麵包。

「妖精的村子，变成艾露西艾後真的变了呢。如果是以往的这个时期，吃的东西没有了，忍耐着饥饿，就算只有一点也好为了避免消耗体力，大家都不会工作在家发抖而已呢」
（脑补，原文：エルフの村、エルシエになって本当に変わったね。いつもならこの時期って、食べるものが無くなってきて、ひもじさを堪えて、少しでもお腹が減らないように、みんな動かずに家で震えて過ごしてたよね）

除了粮食的生产量减少之外，由於得上缴给帝国的税（又把村子给）榨乾了，以往的这个时期，都会为会不会出现饿死的人而担心，对於粮食（不足的问题）困扰着。

「是呢。去年和露西艾两个人，明明太阳公公才刚升起，就因为肚子饿而卷在毛毯裡。好怀念哦」
「然而今年吃的东西有很多，至今为止所不知道的美味东西也在餐桌上并列着，变得能够吃上那麼奢侈的甜食。有时我在想，太过幸福了，现在的生活会不会全部都只是一场梦」

露西艾一边笑着一边把（沾上了）枫糖浆而变甜的麵包放入嘴巴。

「这不是梦哦，是靠着大家一起努力，终於达到这裡的。我不会让至今为止的努力的成果化为乌有的」
「希利路，我和希利路一起住，变得最喜欢希利路什麼的真是太好了」
「为什麼突然之间说这种话」

我在问这个问题时，（露西艾的）脸变得稍微有点寂寞。
（不确定，原文：俺が問いかけると、少しだけ寂しそうな顔をした。）

「最近呢，希利路不再是我的希利路，变成了大家的希利路了，我感觉得（有点）寂寞。
只有我知道真真的希利路的厲害（的地方）。虽然我以前是这样想的（脑补，原文：そんなふうに昔は思ってたけど），现在大家都已经知道希利路的厲害了。从现在起，知道了希利路很厲害的人会出现呢。在那其中的某人会把希利路给抢掉，我有着这样的感觉」

无力感，然後见证过父亲的败北而腐烂的我，（然而却依然）一直相信着我的只有露西艾。正是因为是这样的露西艾所以我才爱着她。

「不管是怎样的孩子出现了，我的第一名永远都会是露西艾哦。再说，要说这种事的话，露西艾也是，身边有群害虫……咳咳（拟态词：ごほんっ），也有喜欢着露西艾的男孩子难道不是吗」
「嗯，有的哦。但是希利路这一边永远都比较帅，（所以）你就放心吧」
「那麼我也是一样的哦。比露西艾还要可爱，心灵更加坚强的女孩子是绝对不存在的，（所以）你（也）放心吧。」

我这麼说，露西艾对我笑了。那个表情是那麼的漂亮，明明是已经看习惯了的脸，心臟却（扑通扑通）地激烈跳动着。

「嗯，果然是梦呢。这麼幸福的日子不可能会来的」

露西艾捏了捏自己的脸。
我对这奇怪的事苦笑了。

「再说，到了现在还说着这种话我会困扰的哦。艾露西艾还能够变得更加丰裕。我会变得更加喜欢露西艾，想变得（更加）喜欢露西艾。
让我来说说这以後的事吧，把酒给酿造出来和大家一起庆祝。赢了和帝国的战鬥，卖了枫糖浆入手一大把的金币。举办盛大的结婚仪式被大家祝福着。增加各种各样新的农作物和家畜，然後制作更多好吃的东西。生一堆可爱的孩子。把艾露西艾扩建的更大。然後，直到死为止一直笑着，（最後）在年老时去世」

这个才是我的梦（想），想和露西艾一起看见的梦（想）。

「哇～（あはは），真好呢。只是想想罢了就觉得幸福了。嗯（うん），最高の人生」
「要是不能相信的话，我（……）」

在我说到一半的时候，露西艾把手指放在我的嘴唇上不让我把话说下去。
（意译，原文：そこまで言いかけたところで俺の唇にルシエの指が当てられ言葉が遮られる）

「不会说做不到的事。对吧？希利路的那番话，已经听过好多次了，（已经）好好地记住了」
「就是那样。所以，希望（你能）相信（我）」
「嗯，我相信你。我最喜欢希利路了，而且，那个梦（想）真好啊，我是打从心底这麼想的」

（我们）两人，在吃完麵包後和大家会合了，存有大量的枫糖浆的水瓶由複数的人搬运着下山了。
由於也能期待下一次的採集，所以在心裡决定把今天的份就用来当做分给大家的份和用来酿造酒的份。
