【这周也是很不容易的上传了。
连续幾天都是炎热的日子啊。自己也是十分注意身体状况的同时继续接着写。】
=================================
「果然，吗……」
继那发现了眼前的这册书籍，开始小声自说自话起来。在和昂相遇的地方突然出现了一个新的入口。踏入其中的继那，像是被引导着一般，向着又暗又狭小的道路前进着。随即不久之後，一本书出现在了眼前的石头台座上。
眼前的台座上，有一册书置於上面。那本书和自身所携带的魔书克苏鲁一样缠绕着浓密的黑色灵气。另外，可以感觉其四周蔓延着一股刺扎皮肤的魔力，魔力的波形也和克苏鲁完全一样。

――魔书选择人。

那是在过去，被多数的先辈们所发现并确立的一个法则。但是冒然说书选择了人什麼的，对其一无所知的人听到这样的话可能只会说道“什麼蠢话”什麼的，然後对此嗤之一笑吧。然而，如果回顾一下歷史的话，那样的蠢话确确实实的正在现实中發生着。
另外，补充在说明一点，历史上同一个人得到「第二册」的事是从来没有听说过的。因为就连记录和传承都没有，这也是没有办法的事啊。当然「第二册」这样的概率性问题也是有其發生背景的。但是，由於自己没有听说过那样的事...继那忍不住开始想象，一个人如果得到了两本魔书，他将会踏上什麼样的不归之路呢？
「……嗯！」
拂去脑海中的想象，继那没有任何思考深深地吐了一口气，不由得咬紧牙关，触及那本书的封面。

瞬间，视野开始模糊――



「呀啊。又见面了呢！」


稍加注意的话，继那和那个似曾相识的小男孩站在宽阔的白色的空间裡。





「嗯？　……啊啊、嗯。是呢」

――“这是错觉吗？” 那样的话语在别的地方也出现过，男子向继那搭话道。
「放心好了。你还没死呢。」
「那还真是感谢。不过、我才不会就这麼简单的死去呢。」
穿着印有白色食物的长袍的男子——就是使继那诞生来到这个世界的人——正太神，他的脸上露出了让人怀念的微笑。


◆◇◆◇◆◇◆◇◆◇◆◇◆◇

「……嗯？　这是什麼情况？」
强忍住各种各样想吐槽的衝动，继那总算是向眼前的正太神询问道。已经适应了这样的情况，没有表现出慌乱举止的继那也可以说是了不起的吧。
「啊哈哈。不要那麼紧张嘛。刚才也已经说了，你并没有死啊。」
「就算你这麼说了我完全感觉不到啊。毕竟这裡是――」
继那一边那麼说着，一边环视四周。出现在视线裡的一直是广阔的纯白的景象。“再见到这个景象是隔了多久呢...”继那如此怀念着。
「哎你要那麼想我也没办法。这裡是和那个冥界不同的，是一个特别的世界。虽然景色看上去很像。因为这次的话稍微有点複杂，我只是将你的意识叫到这裡来。比起这个，我觉得相似的情况之前也有發生过吧？」
「啊...ORZ，这傢伙根本就没有时间的概念，完全不知道那件事之後已经过了有多久了!不过，意识什麼的...」
即使这麼说了，但马上「是吗，这样啊」这样能接受然後继续巴拉巴拉人还是很少的吧！但是，对了经歷过一次转生的继那来说，已经有所觉悟了，「这傢伙的话什麼都有可能啊...」进入了这样一种死心（或是醒悟？）的境地了。
（就算是这个样子好歹也是神啊……。虽然说话有点不靠谱）
「……现在，没有在想些失礼的东西吧？」
「还是一如往常」
继那一本正经的脸避开目不转睛地看过来的正太神的视线。「快点转移到正题上」他用视线催促着。正太神也「好可疑...」地一边发着牢骚，另一边也总算是把步入了正轨。

「总之把我叫过来我是可以理解的。……。那麼理由是什麼呢？」
「这次把你叫过来是想传达关於你的持有魔法及『职责』。」
「我的职责？」
「是的。但是呢，在这之前，你先确认一下自己的『现状』吧。一定發生了些有趣的事情。」
对於总觉得哪裡浮现出恶作剧似的笑容的正太神，继那一边表示怀疑，一边顺从着他的话。於是，发现了在自己的状态——其下层的创造召唤魔法的变化。

――创造召唤魔法　　　　　　　　　Lv.7

?大罪召喚?
-愤怒（Wrath）――龙鄂刀
-暴食(Gluttony)――[条件未开放]　?解放可能?
-嫉妬(Envy)――[条件未开放]
-怠惰(Sloth)――[条件未开放]
-強欲(Greed)――[条件未开放]
-色慾(Lust)――[条件未开放
-高慢(Pride)――[条件未开放]

「―—诶！？　条件...被解放了？　为什麼？」
对於大吃一惊而睁大眼睛的继那，正太神忽地笑起来，并说着「恭喜」。
「那本魔书的名字是『死灵之书』那是作为你的『第二魔书』，为了授予新力量的关键之一。」
「第二魔书？　新力量？」
对於听到的事情没能细细品味，继那就像鹦鹉学舌般小声嘟囔着。难得看到他困惑的样子，正太神一边点头一边絮叨着。
「没错。继那——你的持有魔书？克苏鲁？是在魔书中特别的一册。那就是，『统治魔书？？的魔书』
「统治魔书，是……」
「就是字面上的意思。说起来，魔书是将力量借给寄宿着强大力量的使用者和被认可的人。这世上一共有7本。每本魔书都有自己的名字，『死灵之书』『伊波恩之书』『塞拉伊诺断章』『拉莱耶文本』『断罪之书』『法之书』，再加上『克苏鲁』。这就是所谓的7本魔书全本。其中，「克苏鲁」有着比较特殊的地位，统治着前面的6本...不对，正确的说，克苏鲁的目的就是将那些魔书聚集起来，整理成为一本书的装置。这种说法会更好一点吧？」
（『死灵之书』啊『断罪之书』啊...是哪裡的遊戏吗！？）
正太神所说的魔书的名字。是继那转生前自己最为熟悉的「遊戏和动漫」裡经常出现的代表性东西，继那抱着“是不是哪裡出错了什麼的”的想法。
「……怎麼了？」
「啊，没什麼。只是，不是很能理解第二本魔书呀还有聚集汇总什麼的..为什麼魔书会分开，事到如今还有汇集的必要吗..」
“说的再详细一些。”继那锐利的视线像是在这麼说着。正太神一边露出苦笑，一边微微地点着头。
「我明白了。後面会好好说的...因此，虽然我觉得你应该已经明白了。但是，把这六本魔书汇集起来，是开放你的『条件』的唯一手段。」
「大罪召唤吗？……」
听了正太神的提示说明，呆呆地眺望着「状态」上浮现着的文字的继那像是理解了一般自言自语道。
「那麼，这样就知道了吧……」
「啊啊。说了这麼多话总觉得有点明白了。总而言之，就是想要我集齐这7本魔书系列这麼一回事是吧？」
「――正确」

回想起了让人怀念的往事，正太神脸上闪现出了腹黑的笑容，编织着短短的几句话。
「因为那就是你——佐伯继那在这个世界实现转生的理由，你的职责。」
年幼的孩子神好像露出认真的表情，直直的看着对方的脸，静静地，温柔一般地说着。
【现在，正在慢慢的改稿中。
因为途中的经过报告都记载在活动报告裡，可以的话，还请看一下。】

注：
【死灵之书】(Necronomicon) 出自是恐怖小说家洛夫克拉夫特（H. P. Lovecraft）创作的克苏鲁神话中出现的一本虚构魔典，书中记载了世界上最疯狂最邪恶的事物。
【伊波恩之书】(Book of Eibon) 克苏鲁神话体系中的一本著名魔法书。由希柏里尔(Hyperborea)的大魔法师伊波恩(Eibon)所写。在传说中，这本魔法书记载着早已被人类遗忘的传说，是阴晦悚然的神话、邪恶高深的咒语、仪式与典礼的集大成之作。
【塞拉伊诺断章】(Celaeno Fragments) 克苏鲁神话体系中的魔法书。
【拉莱耶文本】(R'lyeh Text) 克苏鲁神话体系中的魔法书。
【断罪之书】(Liber Damnatus) 克苏鲁神话体系中的魔法书。
【法之书】(Liber AL vel Legis 英: The Book of the Law)
【克苏鲁】(Cthulhu) 克苏鲁是美国小说家霍华德·菲利普·洛夫克拉夫特所创造的克苏鲁神话中的一个邪恶存在。以在克苏鲁命名的克苏鲁神话中，旧日支配者克苏鲁虽然决不是地位最高的，却是最知名的，也是克苏鲁神话的形象代表。
