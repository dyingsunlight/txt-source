１ー３　萝茲与夜间的攻防


在阴暗的房间裡。我的双手双脚都被绑着躺在床上。

[唔咕——！？]
这是什麼情况！？虽然想大声叫出来，但嘴裡却被堵着无法出声。

[呵呵……柚希哥哥，你终於醒了呢]
焦急的我的正上方，降下了天真无邪的声音。

扭头看过去，萝茲那俯视着我的脸就在眼前。一瞬间的混乱之後，发现原来在被萝茲膝枕着。

[尼则死，呀则撒么！？（你这是，要做什麼）]
[呵呵，在说啥完全听不出来呢]
萝茲微笑着，把堵住我嘴巴的东西拿走了。

[这样做，是想幹啥——唔！？]
一开口想让其对此进行解释，又立刻把嘴堵上了。

——只不过，堵住嘴的并不是口塞，而是萝茲那小巧的嘴唇。诶……KISS？被KISS了！？诶，这是啥意思！？
脑裡一片混乱，但四肢都被绑着无法抵抗。

[嗯……啾……哈……]

过了幾十秒，又或是幾分钟。萝茲贪婪地享用完我的唇之後，终於将其放开了。在我和萝茲的唇间，连着一条透明的丝线。
一时间忘了呼吸的我，这时候才赶忙呼吸起空气来。

[哈、哈……萝茲，你幹嘛……？]
[呵呵，这是我的初吻哦。一生都忘不了了吧？]
[就算不这麼说……我这一生都不会忘记的啦]

不知是幸运还是不幸，我这边也是初吻。而且，还是在阴暗的房间四肢都被绑住的情况下，被比我小的金髮美少女强行地狂吻了一通。
这种体验，想忘都忘不了。

[然後呢……为什麼要把我拘束起来？如果你想说明的话，就请把理由讲给我听吧]
由於用粗暴的语气可能会导致再次被塞口球，所以我尽量採用比较温和的口气问她。对此，萝茲露出天真的，真的很天真无邪的微笑。

[都是柚希哥哥的错哦？明明都拜托了留在家裡了，没想到却给拒绝了。所以呢……这也是没办法的呐？]
[哼，事情发展不如愿，就把人关起来了麼]

听了萝茲的话後，我也有点火大了。
事情发展不能如自己所想之後，就强行无视对方的意愿让其按照自己想法走。这种情况，就和我印象裡的那些病娇一个样啊。

还以为萝茲是个会为他人着想的普通女孩……看来我还是看走眼了。因此，对轻信了萝茲的自己感到有点恼火。
萝茲目不转睛地盯着生气的我。

[……幹嘛啦？]
[柚希哥哥，居然没胆怯呢]
[已经很受惊了啊……]

毕竟啊。至今为止我都已经有过不少这样的恐怖体验了，最糟糕的时候被杀了还能復活，也就安心了。
话说回来，我的精神力可是有AAA的而且还有恐怖耐性B，害怕什麼的情绪会被抑制住的吧。实际上，一点都感觉不到害怕。
再说，也没有告诉萝茲的必要因此就这麼糊弄过去了。

[虽然嘴上说着受惊了，但这不是一点都没颤抖嘛。果然，柚希哥哥是最棒的呐。在初次相遇的时候，我就觉得你就是我命中注定的人了哦？]
[命中注定之人，呢……没搞错吧？]
口气变得不快了，不过也确实觉得不爽了。

我由於有着被病娇爱得死去活来：SSS这技能，会影响到病娇的妹子的可能性很高。

如果是纯粹的好意的话我会觉得很高兴……但如果是因为技能的原因有好感的话，那就敬谢不敏了。嘛，反过来说，如果给些不好的影响的话，就真心觉得坑人了。

[明明好不容易能和我在一起了，却总感觉一点都不高兴呢]
[……手脚被绑起来监 禁着，我又没有喜欢这玩法的性 癖呐]
[那是因为，柚希哥哥很强的原因哦。这之後，我可是打算劝说柚希哥哥留在身边的呢，让你自由行动的话万一逃了怎麼办]
[如果我说不会逃走的话，能把拘束解开吗？]
[……一言为定哦？]

萝茲闪着异色光彩的双瞳，死死地盯着我。似乎是想要看透我话语裡的真伪。

不过反过来说，如果能让她相信我的话是真心的，那麼就有可能解开这拘束。
我毫无掩饰地回看着萝茲的眼，点点头表示答应。

[那麼……就算把拘束解开，也会好好听我的话？也不要从这个房间裡出去，说好了哦？]
[啊啊，【?一?言?为?定?】]

许下虚伪约定的那瞬间，有种仿佛出卖了灵魂的感觉。
不过，与其让我过着被病娇妹子饲养在房间裡的生活，还不如一脸淡然地把谎言继续下去呢。不久，萝茲微笑地说着[约好了哦]，把我的四肢解开了。
……真好应付。

不过，内心裡在想的东西一定要小心，不能被她察觉出来，如果着急着逃跑，之後又被抓回来的话那就没啥意义了。
我先撑起身体，然後为了表示没有逃跑意向，说了句[谢谢了，萝茲]，并温柔地摸了摸她的头。
对此，萝茲舒服得眯起了眼。

[诶嘿嘿……柚希哥哥，好舒服哦。其实不只是头髮，其余各种各样的地方都可以摸的哦？]
[……各种各样的地方？]
[嗯。各种各样的地方。柚希哥哥成了我的东西，但同时我也变成了柚希哥哥的东西了。因此，柚希哥哥想做的事，全~都，可以帮你完成哦]
[全、全部？]
[嗯。如果有想让我穿的衣服的话，不管是多大胆的衣服都可以穿给你看的。而且，如果有什麼想对我做的事的话，不管是什麼……事情，都可以哦？]
[这、这麼说……]

不禁咽了口口水。
不对，别想歪了。那个，怎麼说呢，这只是身为男性的正常反应，因为萝茲只看外在也是个超绝美少女那一类的，说着不管是啥——都肯的的话，作为思春期的男生来说总有这样那样的兴趣的嘛……不行不行。要冷静。

要是这样就被诱惑了的话，肯定会落得一生都被饲养在这房里的结局裡了。

[柚希哥哥？]
[这个，怎麼说呢……是个非常有魅力的建议啦，不过我现在稍微有点渴了。那麼，能先让我喝点水可以吗？]
[……口渴了吗？]
[唔、嗯。喉咙干巴巴的了]

虽然只是个制造逃跑机会的接口，但口渴了也是真的。在阴暗密室裡的床上，陷入被美少女诱惑的状况，真的会喉咙乾巴巴的。

[嗯~那麼，我先去拿水来吧。那麼，在此期间，柚希哥哥，考虑下要对我做点什麼哦。【?拜?託?了?呦?】]
[嗯、嗯。会好好考虑的]

我点点头，萝茲就满足地说着[我去拿水过来哦]出了房间。尽管如此，我还是小心谨慎地，确认了一下萝茲萝茲确实远离了这间房间。

……很好，应该已经安全了。

那麼，就在萝茲回来之前，好好考虑下要玩些什麼——啊，不对不对。我可是要逃跑的啊，不在萝茲回来之前逃掉怎麼行。

……不过，要是让萝茲穿上有点H的款式的服装，看着她那害羞得不得了的样子也不错啊。而且，给纯真的萝茲教点各种各样的……

啊不好，现在可不是考虑那种东西的时候！
搞什麼鬼啊，这奇葩的危机感。仔细想想，明明是很糟糕的状况，怎麼一点危机意识都没有。

难道说，是因为恐惧耐性而失去了危机感，然後就输给了诱惑？如果真是如此，或许就要想办法学一下诱惑耐性了。
……不不不，这可不是能考虑些其他事情的悠闲场合。

我下定决心走到萝茲离开的房门前。确认了下房间外没有什麼声音後，把门，慢慢地——打不开！？

啊、啊咧……奇怪了。萝茲出去的时候也没见有开锁的动作，关上的时候也没听到有锁门的声音。门应该没锁起来啊。

不对，这已经不是门锁不锁的问题了。是我的手根本就没有转过门把手。
……难道说，真的是我的本能，让我留在这裡和萝茲做这种那种不可描述的事情？不对……这怎麼可能。再怎麼说，我也没那麼无节操啊。

那这样看来，就只有门被魔法一类的方法锁了起来这种可能了。
详细情况可能不太清楚，但看来从门这边逃出去应该是没可能的了。那这样就没办法了。只能从窗那边逃走了——我立马冲向窗边。

窗这边……没锁！
这样一来就能逃出去了。我在打开的窗往下看，目测窗口离外面地面大概有2米高。随便跳下去可能会扭伤脚，不过最差的情况还有不老不死。（PS：意思是头着地？）

门不行的话就只能冒点险了，我当正将脚踏向窗边的时候——突然生出一股揪心般的恐惧，脚一软就蹲了下去。

[呜——唔，怎、怎麼了。这是……什麼——咕]
身体不住颤抖，呼吸也变得艰难。与恐惧耐性B，精神力AAA之类的能力补正无关，那种恐惧就像疯了一般向我袭来。

——到底抖了多久？门轻而易举地被推开，萝茲拿着上面放了装有水的杯子的托盘回来了。

[我回来啦~……诶，啊咧？柚希哥哥，你幹嘛蹲在那裡啊？]
错过了逃跑的时机了——连思考这种事情的餘裕都没有了。不仅如此，我的本能仿佛盼望着对方回来一般，缠住了萝茲的身体。

[萝茲，萝茲！]
[呀。哥哥，你突然咋了？水都洒了哦]

萝茲手中的托盘掉落在地，杯裡的水洒落在地毯上。可现在的我连在意那个的餘地都没有，只顾着将那娇小柔软的身体紧紧抱住。
仅仅如此，那种要将我压碎般的恐惧，渐渐开始远离了。

可是，在我安心下来的瞬间，萝茲就在我耳边轻声道[难道说……想逃走吗？]。这突如其来的话，令我虎躯一震。

[啊哈，果然如此啊。真是的，这行不通的哦~？『不要从这间房间裡逃走。好好听我的话』，不就已经好~好地定下了契约了麼]
[……契、契约？]
[没错哦。我用魔眼定下的契约]

顺着萝茲右手双指分开，金色的右眼也呈现在面前。单看这个动作是很可爱啦，可是在体会过刚刚那种恐惧之後可没有考虑这个的餘裕。

[那个金色瞳，居然……是魔眼？]
[没错哦。我的魔眼契约的内容，是直到死为止都不会解除，绝对无法违背的。柚希哥哥这麼强，原本还担心会不会不起作用呢，看来有好好运作就安心了]
方才，看着眼睛做约定的那时嗎——我紧咬着唇想。

这个游戏般的世界是有魔法的，萝茲如此炫耀般地展示着她的异色瞳。明明察觉到有些什麼不寻常的力量……早就应该警惕一下的。
不过，现在说什麼都晚了，比起这个还是好好想下今後该怎麼做吧。

和萝茲定下的约定……似乎是[不要从这个房间裡出去][要听萝茲的话]这两个吧。不抵触这两点，在找下离开这个房间的办法吧。
喂……这简直就不可能做到的吧。

不过，要是就这麼放弃了的话，到死为止都要被圈养在这个房间裡。不对……我死了的话会復活的，屈居人下的时间会更久……嗯？等等。

萝茲刚刚说，契约是到死为止都不会解除吧。那麼反过来说，死而复生的话，契约就会失效……？
……姑且先确认一下，於是打开了状态栏。

虽然担心给萝茲看到了会很麻烦……但是看来萝茲那边是看不到我的状态栏的吧。对此貌似没什麼反应。
不过——

[柚希哥哥，虽然不知道你现在在想些什麼，不过也差不多了……呐？]
估计是因为我一直沉默着吧。萝茲的身体稍微移开了一点，仰望着我的脸。看着那与幼小的外表非常相称的水汪汪的眼睛，我不由得咽了口口水。

[差、差不多了，是指啥？]
[当然是继续刚刚的事哦。【?拜?托?你?】触摸我的身体]
萝茲那请求式的命令从口中流出。在那瞬间，我那抱着萝茲手，不受控制般地在萝茲的後背摩擦起来。

[呀……呜、啊……好痒哦]
边说边扭着身子。虽然就如话裡说的那样只是瘙痒般的抚摸而已，但总觉得指的是其他的事（日文里痒和害羞是同一个词），為此我自覺到血氣正上湧。

[呵呵，摸一下其他地方也可以的哦？还是说，想让我来摸呢？]
[不行，别那样！]

糟糟糟糟了，这个非常的不妙啊。要是任由萝茲摆布的话，各种行为会逐渐升级的。如果变成那样了的话，我不觉得还能冷静地思考逃脱的方法啊。

[等下！这个……那啥。还是先让我来吧！]
虽然後悔着自己说了什麼鬼话，但覆水難收。继续说着[最初，还是让我以自己的意思来办吧]。

[按柚希哥哥的意思来？你想让我，做啥呢？]
[那个……我想想啊……对了。我先在床上坐着，然後萝茲再背向我坐上去如何？]
[诶？突然就让我做这个？我觉得，还是一步步慢慢点来可能比较好点……不过，如果柚希哥哥这麼说了的话……]
[不不不不对，只是摸摸身体而已！所以请不要把手伸向裙裡啊！]
拼命阻止了正在脱着什麼的萝茲。然後我就隨意坐在床上，之後让萝茲坐到上面。
從畫面上來說，这样绝对进去了——這樣的一個讓人吐嘈的姿势，却啥都没有进去，也没有吐槽。（PS：吐槽和插入的讀音相同）

总之，我以背後环抱着萝茲的姿势，抚摸着那柔软的腹部与大腿。

[呀~柚希哥哥。总觉得，嗯。好痒啊~~~]
[痒不是肯定的嘛？萝茲你可要做好被玩弄到脑袋一片空白的觉悟哦？]

我将逃跑的心思给压到心底，尽力对萝茲的腹部或大腿，甚至是耳朵和颈部都进行了一番轻抚。

不愧是贵族之女——这种东西我才不管，只是尽情地抚摸着那光滑细腻的肌肤。而且，因瘙痒而浑身颤抖的萝茲很是令人怜爱。

如果她不是个病娇的话……
也就是说，病娇对我来说是没戏的。我一边留着半分注意在无意识动作的手上，一边把视线转到悬浮着的半透明状态栏窗口上。

我本身的状态数值大体上没什麼变化。但是多了个“和萝茲的契约：E”原本残余0的SP增加到了105。
根據LOG日志，救了伯爵千金的奖励点数有100SP，打倒敌人首领有5SP这样。考虑到点数需求……这点点数就相当少了。

顺带一提……还有着喝下了安眠药的日志。让我在晚饭期间躺倒了的，似乎是喝了一杯茶之後的事。

这个姑且不管。……我对能这麼轻描淡写地忽略被安眠药放倒并拘束起来的经过的自己感到有点怕了，不过姑且还是不管了吧。
我打开了萝茲的契约的详细信息。在信息裡，写着刚刚从萝茲那裡听到的契约差不多的内容。

解咒的方法是，由契约主解除，又或者是以比契约还高等级的解咒魔法（DESPELL）解除。还有就是，因契约的对象死亡而解除，这三种就是全部的解咒方法了。

学习DESPELL得花费200SP。因为萝茲的契约是E级，所以解咒需要升到D级……合计需要1200SP。这果断的不够啊。

[呀——好、好痒啊。柚希哥哥……]
浑身颤抖的萝茲的声音，渐渐变得娇艳起来了。我强行将那声音忽略掉，继续把注意力集中到了STATE上。

查看着的是，【被病娇爱得死去活来】这个，以及不知什麼时候新增的三个称号。
首先——我点开了【被病娇爱得死去活来】的详细信息。

【被病娇爱得死去活来】
增幅病娇对自己的好意。

F：没有补正　/　E：5%　/　D：10%　/　C：15%　/　B：20%　/　A：25%
　AA：35%　/　AAA：45%　/　S：60%　/　SS：75%　/　SSS：100%

【E级效果：吸引病娇的目光+10%】【A级效果：增幅对自己产生好意的对象的病娇属性+10%】【S级效果：引出附近的人潜在的病娇属性+10%】【SSS级效果：增幅附近的人的病娇属性+10%】

喔呜……这EX技能。萝茲之所以会突然病娇化，该不会就是这个技能搞的吧？

不过，引出病娇属性的补正也没那麼大啊。
就算是满足对我有好感的前提也只是20%。虽然也有刚好中獎的可能性，但原本就是病娇的可能性更高。

[柚、柚希哥哥，我、真的——呀~]

无视掉萝茲因腋窝被瘙痒而产生的悲鸣。然後，被病娇爱的死去活来的详细研究也先推後。先看看可能对现状有用的其他东西。
新的称号……有三个啊。

【蒙受女神美狄亞的恩寵了】的内容说的是，所有技能效果提高10%，连亲密的对象的STATE也能干涉。
虽然对干涉STATE这点稍微有些在意，但既然写了亲密的对象，那应该对现状没啥用。那麼就跳到下一个吧。

【來自異世界的旅人】和【被病嬌愛到死去了】共通的效果分别是，全STATE增加5%和2%。

顺带一提【被女神美狄亚一见钟情了】的补正是10%，所以总共有27%的补正。
还有的就是力量补正有10%，因此力量增加了37%。速度方面也有类似的东西增加，所以这应该就是轻鬆地击退袭击犯的原因吧。

[柚希哥哥、柚希哥哥！我、我要，不、不行了~~~~]
萝茲那原本在忍着什麼的身体一阵抽搐，然後瘫软下来。好像有点欺负过头了，我连忙撑起她的身体。

[……没事吧？]
[哈呜……柚希哥哥，好痒，哦……全身都被摸遍了，脑袋裡，晕乎乎的，感觉、快要出、事了……]
[唔嗯。那……已经不想再摸摸了麼？]
[那、那个……如果柚希哥哥，还想摸的话……那麼……可以哦？]

脸蛋泛红的萝茲，飘忽着视线轻声说道。还在逞强——尽管如此，但那垂涎欲滴的表情真是讨人喜欢。

虽然觉得会对这边产生好感是由於【被病娇爱得死去活来】的缘故，但考虑到是SSS的水平，这效果估计要翻倍了。
效果翻倍听起来是很厲害，不过如果原本就没有好意的话那就啥效果都出不来了。

仔细想了想，萝茲应该有一半是带着好意的吧……怎麼说，有点不好意思了。啊，并不是因为瘙痒搞垮身体的缘故哦。
这麼想着的时候，萝茲扭扭捏捏地扭过了身子。

[那、那个。柚希哥哥。我，我想……去换下衣服，可以吗？]
[换衣服？啊……嗯。去吧]
因为还要分析一些事情，所以速度地把萝茲送出房间了。然後确认了萝茲远离房间了之後。我握紧了拳头。

萝茲不知道去了哪裡正在换衣服。说是如此，感觉不会全部都换完，所以虽然时间上可能不会花太久，但应该还是有点缓冲的时间吧。

因此——我快速地打开了技能学习界面。然後在列表里筛选要学的魔法。然後找到的是，100点SP就可以学习的火球术（fire ball）。

使用的方法是，想象要攻击的目标，然後宣告技能名。做完後这技能会在地上展开魔法阵，展开结束後就会发动——流程就是如此。

学了之後就能用是个好事，但F级也有不能一边动一边用的各种毛病。话虽如此，对於现在来说只要能用就没问题了。
我立马就把火球术：F给学了。
